package edu.ntnu.idatt2001.eilertwh.model.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;

/**
 * The type Assassin unit.
 * @author Eilert Werner Hansen
 */
public class AssassinUnit extends Unit {
    /**
     * Instantiates a new Assassin unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 and if name is blank or contains comma
     */
    public AssassinUnit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        super(name, health, attack, armor);
    }

    /**
     * Instantiates a new Assassin unit.
     *
     * @param name   the name
     * @param health the health
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 and if name is blank or contains comma
     */
    public AssassinUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, 20, 5);
    }

    /**
     * gets unit type
     *
     * @return unit type
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.ASSASSIN_UNIT;
    }

    /**
     * Gets attack bonus.
     *
     * @param terrain the terrain
     * @return the attack bonus of the unit
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int terrainBonus = 0;
        switch (terrain){
            case ICEBERG -> terrainBonus = -1;
            case FOREST -> terrainBonus = 3;
        }
        return 2 + terrainBonus;
    }

    /**
     * Gets resist bonus.
     *
     * @param terrain the terrain
     * @return the resist bonus of the unit
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        int terrainBonus = 0;
        switch (terrain){
            case DESERT -> terrainBonus = 2;
            case ICEBERG, VOLCANO -> terrainBonus = -1;
        }
        return 1 + terrainBonus;
    }
}
