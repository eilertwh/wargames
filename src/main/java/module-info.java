module edu.ntnu.idatt2001.eilertwh {
    requires javafx.graphics;
    requires javafx.fxml;
    requires javafx.controls;


    opens edu.ntnu.idatt2001.eilertwh.client.controller to javafx.fxml;
    exports edu.ntnu.idatt2001.eilertwh.client.controller;
    exports edu.ntnu.idatt2001.eilertwh.client;
    opens edu.ntnu.idatt2001.eilertwh.client to javafx.fxml;
}