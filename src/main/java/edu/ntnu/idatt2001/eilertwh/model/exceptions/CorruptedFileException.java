package edu.ntnu.idatt2001.eilertwh.model.exceptions;

/**
 * The type Corrupted file exception.
 * @author Eilert Werner Hansen
 */
public class CorruptedFileException extends Exception{
    /**
     * Instantiates a new Corrupted file exception.
     *
     * @param message the message
     */
    public CorruptedFileException(String message) {
        super(message);
    }
}
