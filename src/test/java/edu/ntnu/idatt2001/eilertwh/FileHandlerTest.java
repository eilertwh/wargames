package edu.ntnu.idatt2001.eilertwh;

import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.exceptions.CorruptedFileException;
import edu.ntnu.idatt2001.eilertwh.model.exceptions.UnsupportedFileExtensionException;
import edu.ntnu.idatt2001.eilertwh.model.army.Army;
import edu.ntnu.idatt2001.eilertwh.model.filehandler.FileHandler;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;

import static edu.ntnu.idatt2001.eilertwh.model.filehandler.FileHandler.*;
import static org.junit.jupiter.api.Assertions.*;

public class FileHandlerTest {
    @TempDir
    File tempdir;

    @Nested
    public class Exception_tests {

        @Test
        public void nullPointerException_thrown_write_to_file() {
            File testFile = new File(tempdir, "testFile.csv");
            try {
                FileHandler.writeToFile(null, testFile.getPath());
                fail();
            } catch (IOException | UnsupportedFileExtensionException e) {
                fail();
            } catch (NullPointerException e) {
                assertEquals("Army cannot be null", e.getMessage());
            }
        }

        @Test
        public void nullPointerException_thrown_sorted_write_to_file() {
            File testFile = new File(tempdir, "testFile.csv");
            try {
                sortedWriteToFile(null, testFile.getPath());
                fail();
            } catch (IOException | UnsupportedFileExtensionException e) {
                fail();
            } catch (NullPointerException e) {
                assertEquals("Army cannot be null", e.getMessage());
            }
        }

        @Test
        public void unsupportedFileExtensionException_thrown_write_to_file() {
            Army army = new Army("army");
            File testFile = new File(tempdir, "testFile.png");
            try {
                FileHandler.writeToFile(army, testFile.getPath());
                fail();
            } catch (IOException e) {
                fail();
            } catch (UnsupportedFileExtensionException e) {
                assertEquals("File must have csv file extension", e.getMessage());
            }
        }

        @Test
        public void unsupportedFileExtensionException_thrown_sorted_write_to_file() {
            Army army = new Army("army");
            File testFile = new File(tempdir, "testFile.png");
            try {
                sortedWriteToFile(army, testFile.getPath());
                fail();
            } catch (IOException e) {
                fail();
            } catch (UnsupportedFileExtensionException e) {
                assertEquals("File must have csv file extension", e.getMessage());
            }
        }

        @Test
        public void unsupportedFileExtensionException_read_from_file() {
            File testFile = new File(tempdir, "testFile.png");
            try {
                Army army = FileHandler.readFromFile(testFile.getPath());
                fail();
            } catch (IOException | CorruptedFileException e) {
                fail();
            } catch (UnsupportedFileExtensionException e) {
                assertEquals("File must have csv file extension", e.getMessage());
            }
        }
    }

    @Nested
    public class Corrupted_file_test {

        @Test
        public void no_name_test() {
            File testFile = new File(tempdir, "testFile.csv");
            try (FileWriter fw = new FileWriter(testFile.getPath())) {
                fw.write("InfantryUnit,brage,10");
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                Army army = readFromFile(testFile.getPath());
                fail();
            } catch (UnsupportedFileExtensionException | IOException e) {
                fail();
            } catch (CorruptedFileException e) {
                assertEquals("Army name not found", e.getMessage());
            }
        }

        @Test
        public void corrupt_Unit_test() {
            File testFile = new File(tempdir, "testFile.csv");
            try (FileWriter fw = new FileWriter(testFile.getPath())) {
                fw.write("brage\nInfantryUnit,brage,10,10");
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                Army army = readFromFile(testFile.getPath());
                fail();
            } catch (UnsupportedFileExtensionException | IOException e) {
                fail();
            } catch (CorruptedFileException e) {
                assertEquals("file contains corrupted Units", e.getMessage());
            }
        }

        @Test
        public void unsupported_unit_test() {
            File testFile = new File(tempdir, "testFile.csv");
            try (FileWriter fw = new FileWriter(testFile.getPath())) {
                fw.write("brage\nDancingUnit,brage,10");
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                Army army = readFromFile(testFile.getPath());
                fail();
            } catch (UnsupportedFileExtensionException | IOException e) {
                fail();
            } catch (CorruptedFileException e) {
                assertEquals("file contains unsupported units", e.getMessage());
            }
        }

        @Test
        public void corrupt_Unit_health_test() {
            File testFile = new File(tempdir, "testFile.csv");
            try (FileWriter fw = new FileWriter(testFile.getPath())) {
                fw.write("brage\nInfantryUnit,brage,f,");
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                Army army = readFromFile(testFile.getPath());
                fail();
            } catch (UnsupportedFileExtensionException | IOException e) {
                fail();
            } catch (CorruptedFileException e) {
                assertEquals("file contains units with corrupted Health", e.getMessage());
            }
        }
    }

    @Nested
    public class read_equals_written_tests {
        File testFile = new File(tempdir, "testFile.csv");
        Army army;

        @BeforeEach
        private void createArmy() {
            army = new Army("lolo");
            army.addAll(UnitFactory.createUnits(100,UnitType.INFANTRY_UNIT,"Brage",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.CAVALRY_UNIT,"Carl",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.COMMANDER_UNIT,"Nicolai",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.RANGED_UNIT,"Callum",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.ASSASSIN_UNIT,"Runar",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.POLAR_BEAR_UNIT,"Trym",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.SPEARMAN_UNIT,"Tomas",10));
         }


        @Test
        public void read_army_equals_written_army() {

            try {
                writeToFile(army, testFile.getPath());
            } catch (IOException | UnsupportedFileExtensionException e) {
                fail();
            }

            Army army2 = null;
            try {
                army2 = readFromFile(testFile.getPath());
            } catch (UnsupportedFileExtensionException | IOException | CorruptedFileException e) {
                fail();
            }

            assertEquals(army, army2);

        }

        @Test
        public void read_army_equals_sorted_written_army() {

            try {
                sortedWriteToFile(army, testFile.getPath());
            } catch (IOException | UnsupportedFileExtensionException e) {
                fail();
            }

            Army army2 = null;
            try {
                army2 = readFromFile(testFile.getPath());
            } catch (UnsupportedFileExtensionException | IOException | CorruptedFileException e) {
                fail();
            }

            assertEquals(army, army2);

            testFile.delete();

        }
    }
}
