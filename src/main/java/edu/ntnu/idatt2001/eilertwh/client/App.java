package edu.ntnu.idatt2001.eilertwh.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;


import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * main class used to launch application
 * @author Eilert Werner Hansen
 */
public class App extends Application {

    private static Stage startStage;
    private static Stage createStage;


    /**
     * starts Wargames stage
     * @param stage start stage
     */
    @Override
    public void start(Stage stage){
        
        startStage = stage;
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getClassLoader().getResource("view/Wargames.fxml"));

        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(),1280,720);
        } catch (IOException e) {
            e.printStackTrace();
        }

        startStage.getIcons().add(new Image("Wargames.png"));
        startStage.setTitle("Wargames");

        startStage.setMinWidth(1280);
        startStage.setMinHeight(720);

        startStage.setOnCloseRequest(e -> Platform.exit());

        startStage.setScene(scene);
        startStage.show();

    }

    /**
     * starts create army stage
     */
    public static void createArmy(){

        createStage = new Stage();

        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getClassLoader().getResource("view/CreateArmy.fxml"));
        Scene scene = null;
        try {
            scene = new Scene(fxmlLoader.load(),800,530);
        } catch (IOException e) {
            e.printStackTrace();
        }

        createStage.getIcons().add(new Image("Wargames.png"));
        createStage.setTitle("Wargames");


        createStage.setResizable(false);

        createStage.setScene(scene);
        createStage.show();
    }

    /**
     * Gets start stage.
     *
     * @return the start stage
     */
    public static Stage getStartStage() {
        return startStage;
    }

    /**
     * Gets create stage.
     *
     * @return the create stage
     */
    public static Stage getCreateStage() {
        return createStage;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {
        launch();
    }
}
