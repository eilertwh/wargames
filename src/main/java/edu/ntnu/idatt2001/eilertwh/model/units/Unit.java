package edu.ntnu.idatt2001.eilertwh.model.units;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;

/**
 * The type Unit.
 * abstract class
 *
 * @author Eilert Werner Hansen
 */
public abstract class Unit {

    private final String name;
    private int health;
    private final int attack;
    private final int armor;


    /**
     * Instantiates a new Unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 and if name is blank or contains comma
     */
    public Unit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("UNit name cannot be blank");
        if (name.contains(",")) throw new IllegalArgumentException("Unit name cannot contain comma");
        if (health <= 0) throw new IllegalArgumentException("health cannot be less than zero");
        if (attack < 0) throw new IllegalArgumentException("attack cannot be negative");
        if (armor < 0) throw new IllegalArgumentException("armor cannot be negative");
        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Attack.
     * methods for attacking an opponent
     * if attack is less than defence the unit will hurt itself
     *
     * @param opponent the opponent
     * @param terrain  the terrain
     * @return damage done in the attack, if negative the unit have hurt itself
     */
    public int attack(Unit opponent,Terrain terrain) {
        int totalAttack = this.getAttack() + this.getAttackBonus(terrain);
        int totalDefence = opponent.getArmor() + opponent.getResistBonus(terrain);
        if (totalAttack < totalDefence) {
            this.setHealth(this.getHealth() - totalAttack);
            return - totalAttack ;
        }
        opponent.setHealth(opponent.getHealth() - totalAttack + totalDefence);
        return  totalAttack - totalDefence;
    }

    /**
     * Gets name.
     *
     * @return the name of the unit
     */
    public String getName() {
        return name;
    }

    /**
     * Gets health.
     *
     * @return the health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Gets attack.
     *
     * @return the attack of the unit
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Gets armor.
     *
     * @return the armor of the unit
     */
    public int getArmor() {
        return armor;
    }


    /**
     * Sets health.
     *
     * @param health the health of the unit
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * toString
     * @return String
     */
    @Override
    public String toString() {
        return getClass().getSimpleName() + ", " + getName() + ", " + getHealth();
    }

    /**
     * gets unit type
     *
     * @return unit type
     */
    public abstract UnitType getUnitType();


    /**
     * Gets attack bonus.
     *
     * @param terrain the terrain
     * @return the attack bonus of the unit
     */
    public abstract int getAttackBonus(Terrain terrain);

    /**
     * Gets resist bonus.
     *
     * @param terrain the terrain
     * @return the resist bonus the unit
     */
    public abstract int getResistBonus(Terrain terrain);
}
