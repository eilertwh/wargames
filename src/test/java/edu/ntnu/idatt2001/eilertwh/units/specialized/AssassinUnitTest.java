package edu.ntnu.idatt2001.eilertwh.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.AssassinUnit;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class AssassinUnitTest {

    @Nested
    public class Get_bonus_test {

        @Test
        public void assassin_unit_get_attack_bonus() {
            AssassinUnit assassinUnit = (AssassinUnit) UnitFactory.createUnit(UnitType.ASSASSIN_UNIT,"Brage", 10);
            assertEquals(2, assassinUnit.getAttackBonus(Terrain.PLAINS));
        }

        @Test
        public void iceberg_assassin_unit_get_attack_bonus() {
            AssassinUnit assassinUnit = (AssassinUnit) UnitFactory.createUnit(UnitType.ASSASSIN_UNIT,"Brage", 10);
            assertEquals(1, assassinUnit.getAttackBonus(Terrain.ICEBERG));
        }

        @Test
        public void forest_assassin_unit_get_attack_bonus() {
            AssassinUnit assassinUnit = (AssassinUnit) UnitFactory.createUnit(UnitType.ASSASSIN_UNIT,"Brage", 10);
            assertEquals(5, assassinUnit.getAttackBonus(Terrain.FOREST));
        }

        @Test
        public void assassin_unit_get_resist_bonus() {
            AssassinUnit assassinUnit = (AssassinUnit) UnitFactory.createUnit(UnitType.ASSASSIN_UNIT,"Brage", 10);
            assertEquals(1, assassinUnit.getResistBonus(Terrain.PLAINS));
        }

        @Test
        public void dessert_assassin_unit_get_resist_bonus() {
            AssassinUnit assassinUnit = (AssassinUnit) UnitFactory.createUnit(UnitType.ASSASSIN_UNIT,"Brage", 10);
            assertEquals(3, assassinUnit.getResistBonus(Terrain.DESERT));
        }

        @Test
        public void iceberg_assassin_unit_get_resist_bonus() {
            AssassinUnit assassinUnit = (AssassinUnit) UnitFactory.createUnit(UnitType.ASSASSIN_UNIT,"Brage", 10);
            assertEquals(0, assassinUnit.getResistBonus(Terrain.ICEBERG));
        }

        @Test
        public void volcano_assassin_unit_get_resist_bonus() {
            AssassinUnit assassinUnit = (AssassinUnit) UnitFactory.createUnit(UnitType.ASSASSIN_UNIT,"Brage", 10);
            assertEquals(0, assassinUnit.getResistBonus(Terrain.VOLCANO));
        }


    }
}
