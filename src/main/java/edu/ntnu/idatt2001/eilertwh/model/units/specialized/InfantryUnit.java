package edu.ntnu.idatt2001.eilertwh.model.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;

/**
 * The type Infantry unit.
 * @author Eilert Werner Hansen
 */
public class InfantryUnit extends Unit {

    /**
     * IInstantiates a new Infantry unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 or name is blank
     */
    public InfantryUnit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        super(name, health, attack, armor);
    }

    /**
     * gets unit type
     *
     * @return unit type
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.INFANTRY_UNIT;
    }

    /**
     * Instantiates a new Infantry unit.
     *
     * @param name   the name
     * @param health the health
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 or name is blank
     */
    public InfantryUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, 15, 10);
    }

    /**
     * Gets attack bonus.
     *
     * @param terrain the terrain
     * @return the attack bonus of the unit
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int terrainBonus = 0;
        if (terrain == Terrain.FOREST) terrainBonus = 2;


        return 2 + terrainBonus;
    }

    /**
     * Gets resist bonus.
     *
     * @param terrain the terrain
     * @return the resist bonus of the unit
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        int terrainBonus = 0;
        switch (terrain){
            case FOREST -> terrainBonus = 2;
            case VOLCANO -> terrainBonus = -1;
        }
        return 1 + terrainBonus;
    }
}
