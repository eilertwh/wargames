package edu.ntnu.idatt2001.eilertwh.model.enums;

/**
 * enum representing Speed.
 * @author Eilert Werner Hansen
 */
public enum Speed {
    /**
     * Slow speed.
     */
    SLOW(0.5,"\uD83D\uDC22   Slow"),
    /**
     * Medium speed.
     */
    MEDIUM(0.01,"\uD83D\uDC07   Medium"),
    /**
     * Fast speed.
     */
    FAST(0.0001,"\uD83D\uDE85   Fast"),
    /**
     * Instant speed.
     */
    INSTANT(0,"⚡    Instant");

    private final double rate;
    private final String symbol;

    Speed(double rate,String symbol) {
        this.rate = rate;
        this.symbol = symbol;
    }

    /**
     * Gets rate.
     *
     * @return the rate
     */
    public double getRate() {
        return rate;
    }

    /**
     * toString
     * @return toString
     */
    @Override
    public String toString() {
        return symbol;
    }
}
