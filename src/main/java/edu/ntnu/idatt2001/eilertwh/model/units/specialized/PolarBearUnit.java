package edu.ntnu.idatt2001.eilertwh.model.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;

/**
 * The type Polar bear unit.
 * @author Eilert Werner Hansen
 */
public class PolarBearUnit extends Unit {

    private int timesAttacked = 0;


    /**
     * Instantiates a new Polar bear unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 and if name is blank or contains comma
     */
    public PolarBearUnit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        super(name, health, attack, armor);
    }

    /**
     * gets unit type
     *
     * @return unit type
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.POLAR_BEAR_UNIT;
    }

    /**
     * Instantiates a new Polar bear unit.
     *
     * @param name   the name
     * @param health the health
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 and if name is blank or contains comma
     */
    public PolarBearUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, 40 , 20);
    }

    /**
     * Gets attack bonus.
     *
     * @param terrain the terrain
     * @return the attack bonus of the unit
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        timesAttacked++;
        int terrainBonus = 0;
        if (terrain == Terrain.ICEBERG) terrainBonus = 2;

        if (timesAttacked == 1) return 6 + 2 + terrainBonus;
        return 2 + terrainBonus;
    }

    /**
     * Gets resist bonus.
     *
     * @param terrain the terrain
     * @return the resist bonus of the unit
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        int terrainBonus = 0;
        switch (terrain){
            case ICEBERG -> terrainBonus = 2;
            case DESERT, VOLCANO -> terrainBonus = -2;
        }
        if (timesAttacked < 1) return 2 + terrainBonus;
        return 6 + terrainBonus;
    }
}
