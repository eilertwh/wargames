package edu.ntnu.idatt2001.eilertwh.client.controller;

import edu.ntnu.idatt2001.eilertwh.client.App;
import edu.ntnu.idatt2001.eilertwh.client.ErrorWindow;
import edu.ntnu.idatt2001.eilertwh.model.army.Army;
import edu.ntnu.idatt2001.eilertwh.model.filehandler.FileHandler;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.exceptions.CorruptedFileException;
import edu.ntnu.idatt2001.eilertwh.model.exceptions.UnsupportedFileExtensionException;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * controller for CreateArmy.fxml
 * @author Eilert Werner Hansen
 */
public class CreateArmyController  implements Initializable {


    @FXML
    private Spinner<Integer> unitAmount = new Spinner<>();

    @FXML
    private Spinner<Integer> unitHealth = new Spinner<>();

    @FXML
    private ListView<Unit> unitList;

    @FXML
    private TextField unitName;

    @FXML
    private ComboBox<UnitType> unitType;

    @FXML
    private Label commanderUnits,assassinUnits,cavalryUnits,infantryUnits,polarBearUnits,rangedUnits,spearmanUnits,totalUnits;

    private ObservableList<Unit> units;

    /**
     * Add unit to unit list
     */
    @FXML
    void addUnits() {
        String name = unitName.getText();
        if (name.isBlank()) name = "Unit";
        units.addAll(UnitFactory.createUnits(unitAmount.getValue(),unitType.getValue(),name,unitHealth.getValue()));
        updateUnitInfo();
    }

    /**
     * Removes units from Unit list
     */
    @FXML
    void remove() {
        int health = unitHealth.getValue();
        int amount = unitAmount.getValue();
        AtomicInteger index = new AtomicInteger(0);
        if (unitName.getText().isBlank()) units.removeIf(u -> u.getUnitType() == unitType.getValue() && u.getHealth() == health && index.getAndIncrement() <amount);
        else units.removeIf(u -> u.getName().equals(unitName.getText()) && u.getUnitType() == unitType.getValue() && u.getHealth() == health && index.getAndIncrement() <amount);
        updateUnitInfo();
    }

    /**
     * Imports army to be edited
     */
    @FXML
    void importArmy() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select army from file");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        File file = fileChooser.showOpenDialog(App.getCreateStage());
        if (file !=null) {
            try {
                List<Unit> importedUnits = FileHandler.readFromFile(file.getPath()).getUnits();
                units.clear();
                units.addAll(importedUnits);
                updateUnitInfo();
            } catch (CorruptedFileException | UnsupportedFileExtensionException | IOException e) {
                ErrorWindow.giveError(e.getMessage());
            }
        }

    }

    /**
     * method used to update unit count labels
     */
    private void updateUnitInfo(){
        totalUnits.setText("Total units:            " + units.size());
        commanderUnits.setText("commander Units: " + units.stream().filter(u -> u instanceof CommanderUnit).count());
        infantryUnits.setText("Infantry Units:      " + units.stream().filter(u -> u instanceof InfantryUnit).count());
        rangedUnits.setText("Ranged Units:         " + units.stream().filter(u -> u instanceof RangedUnit).count());
        cavalryUnits.setText("Cavalry Units:       " + units.stream().filter(u -> u instanceof CavalryUnit && !(u instanceof CommanderUnit)).count());
        assassinUnits.setText("Assassin Units:         " + units.stream().filter(u -> u instanceof AssassinUnit).count());
        polarBearUnits.setText("Polar Bear Units:   " + units.stream().filter(u -> u instanceof PolarBearUnit).count());
        spearmanUnits.setText("Spearman Units:     " +  units.stream().filter(u -> u instanceof SpearmanUnit).count());
    }

    /**
     * Saves new army to file.
     */
    @FXML
    void saveToFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select army from file");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        File file = fileChooser.showSaveDialog(App.getCreateStage());
        if (file != null) {
            String armyName = file.getName().replaceFirst("[.][^.]+$", "");
            try {
                FileHandler.writeToFile(new Army(armyName, units), file.getPath());
            } catch (IOException | UnsupportedFileExtensionException | IllegalArgumentException e) {
                ErrorWindow.giveError(e.getMessage());
            }
        }
    }

    /**
     * method used to initialize scene
     * @param url the url
     * @param resourceBundle the resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        units = FXCollections.observableList(new ArrayList<>());
        unitType.getItems().setAll(UnitType.values());
        unitType.setValue(UnitType.INFANTRY_UNIT);
        SpinnerValueFactory<Integer> amountValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 9999);
        SpinnerValueFactory<Integer> healthValueFactory = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 9999);
        amountValueFactory.setValue(25);
        healthValueFactory.setValue(10);
        unitAmount.setValueFactory(amountValueFactory);
        unitHealth.setValueFactory(healthValueFactory);
        unitList.setItems(units);
        updateUnitInfo();
    }
}
