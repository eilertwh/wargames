package edu.ntnu.idatt2001.eilertwh.model.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;

/**
 * The type Spearman unit.
 * @author Eilert Werner Hansen
 */
public class SpearmanUnit extends Unit {

    private int timesAttacked = 0;

    /**
     * Instantiates a new Spearman unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 and if name is blank or contains comma
     */
    public SpearmanUnit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        super(name, health, attack, armor);
    }

    /**
     * gets unit type
     *
     * @return unit type
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.SPEARMAN_UNIT;
    }

    /**
     * Instantiates a new Spearman unit.
     *
     * @param name   the name
     * @param health the health
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 and if name is blank or contains comma
     */
    public SpearmanUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, 1, 8);
    }

    /**
     * Gets attack bonus.
     *
     * @param terrain the terrain
     * @return the attack bonus of the unit
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        timesAttacked++;
        int terrainBonus = 0;
        switch (terrain){
            case HILL, ICEBERG -> terrainBonus = 4;
            case FOREST -> terrainBonus = -10;
        }
        if (timesAttacked == 1) return 50 + terrainBonus;
        return 0;
    }

    /**
     * Gets resist bonus.
     *
     * @param terrain the terrain
     * @return the resist bonus of the unit
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        int terrainBonus = 0;
        if (terrain == Terrain.VOLCANO) terrainBonus = -1;
        return 1 + terrainBonus;
    }
}
