package edu.ntnu.idatt2001.eilertwh.client.controller;

import edu.ntnu.idatt2001.eilertwh.client.App;
import edu.ntnu.idatt2001.eilertwh.client.ErrorWindow;
import edu.ntnu.idatt2001.eilertwh.model.army.Army;
import edu.ntnu.idatt2001.eilertwh.model.battle.Battle;
import edu.ntnu.idatt2001.eilertwh.model.enums.Speed;
import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.exceptions.CorruptedFileException;
import edu.ntnu.idatt2001.eilertwh.model.exceptions.UnsupportedFileExtensionException;
import edu.ntnu.idatt2001.eilertwh.model.filehandler.FileHandler;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.util.Duration;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * controller for the Wargames fxml
 * @author Eilert Werner Hansen
 */
public class WargamesController implements Initializable {

    @FXML
    private List<Label> infantryUnits;

    @FXML
    private List<Label> totalUnits;

    @FXML
    private List<Label> rangedUnits;

    @FXML
    private List<Label> cavalryUnits;

    @FXML
    private List<Label> commanderUnits;

    @FXML
    private List<Label> assassinUnits;

    @FXML
    private List<Label> polarBearUnits;

    @FXML
    private List<Label> spearmanUnits;

    @FXML
    private List<Label> armyNames;

    @FXML
    private List<Label> armyPaths;

    @FXML
    private Label Winner;

    @FXML
    private List<ListView<Unit>> unitLists;

    @FXML
    private BorderPane background;

    @FXML
    private ListView<String> log;

    @FXML
    private ComboBox<Terrain> terrain;

    @FXML
    private ComboBox<Speed> speed;

    @FXML
    private Button reloadButton, simulateButton, importArmyOneButton, importArmyTwoButton;

    private final Army[] army = new Army[2];

    /**
     * Import army one.
     */
    @FXML
    void importArmyOne() {
        importArmy(0);
    }

    /**
     * Import army two.
     */
    @FXML
    void importArmyTwo() {
        importArmy(1);
    }

    /**
     * opens create army stage
     */
    @FXML
    void createArmy() {
        App.createArmy();
    }

    /**
     * method to disable buttons during simulation
     * @param isDisabled the state of disabled
     */
    private void disableButtons(Boolean isDisabled){
        simulateButton.setDisable(isDisabled);
        reloadButton.setDisable(isDisabled);
        terrain.setDisable(isDisabled);
        importArmyOneButton.setDisable(isDisabled);
        importArmyTwoButton.setDisable(isDisabled);
        speed.setDisable(isDisabled);

    }

    /**
     * starts simulation battle between armies
     */
    @FXML
    void onSimulate() {
        try {
            Battle battle = new Battle(army[0],army[1],terrain.getValue());

            log.setItems((ObservableList<String>) battle.getBattleLog());

            if (speed.getValue() == null) {
                ErrorWindow.giveError("You must select a speed");
                return;
            }

            disableButtons(true);

            if (speed.getValue() == Speed.INSTANT){
                battle.simulate();
                log.scrollTo(log.getItems().size());
                updateUnitInfo(0);
                updateUnitInfo(1);
                Winner.setText(battle.getWinner().getName() + " Wins!!!");
                Winner.setVisible(true);
                disableButtons(false);
            } else {
                Timeline timeline = new Timeline();
                timeline.setCycleCount(Timeline.INDEFINITE);
                timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(speed.getValue().getRate()), actionEvent -> {
                    if (!battle.isFinished()) {
                        battle.singleSimulate();
                        log.scrollTo(log.getItems().size());
                        updateUnitInfo(0);
                        updateUnitInfo(1);
                    } else {
                        timeline.stop();
                        Winner.setText(battle.getWinner().getName() + " Wins!!!");
                        Winner.setVisible(true);
                        disableButtons(false);
                    }
                }));
                timeline.playFromStart();
            }
        } catch (IllegalArgumentException | UnsupportedOperationException e){
            ErrorWindow.giveError(e.getMessage());
        } catch (NullPointerException e) {
            if (e.getMessage().equals("Terrain cannot be null")) ErrorWindow.giveError("You must select a terrain");
            else ErrorWindow.giveError("You must import armies first.");
        }
    }

    /**
     * Reloads army from filepath
     */
    @FXML
    void reload() {
        try {
            army[0] = FileHandler.readFromFile(armyPaths.get(0).getText());
            army[1] = FileHandler.readFromFile(armyPaths.get(1).getText());
            updateUnitInfo(0);
            updateUnitInfo(1);
            unitLists.get(0).setItems((ObservableList<Unit>) army[0].getUnits());
            unitLists.get(1).setItems((ObservableList<Unit>) army[1].getUnits());
            Winner.setVisible(false);
            log.getItems().setAll(new ArrayList<>());
        } catch (UnsupportedFileExtensionException e) {
            ErrorWindow.giveError("You must import armies first.");
        } catch (IOException | CorruptedFileException e) {
            ErrorWindow.giveError(e.getMessage());
        }
    }

    /**
     * method used to update unit count labels
     * @param n army labels to be updated, 0 is army one, 1 is army two
     */
    private void updateUnitInfo(int n){
        totalUnits.get(n).setText("Total units:                 " + army[n].getUnits().size());
        commanderUnits.get(n).setText("commander Units:      " + army[n].getCommanderUnits().size());
        infantryUnits.get(n).setText("Infantry Units:           " + army[n].getInfantryUnits().size());
        rangedUnits.get(n).setText("Ranged Units:              " + army[n].getRangedUnits().size());
        cavalryUnits.get(n).setText("Cavalry Units:            " + army[n].getCavalryUnits().size());
        assassinUnits.get(n).setText("Assassin Units:              " + army[n].getAssassinUnit().size());
        polarBearUnits.get(n).setText("Polar Bear Units:        " + army[n].getPolarBearUnit().size());
        spearmanUnits.get(n).setText("Spearman Units:          " + army[n].getSpearmanUnit().size());
        unitLists.get(n).refresh();
    }

    /**
     * method used to import army
     * @param n army to be imported, 0 is army one, 1 is army two
     */
    private void importArmy(int n)  {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select army from file");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("CSV", "*.csv"));
        File file = fileChooser.showOpenDialog(App.getStartStage());
        if (file !=null) {
            try {
                army[n] = FileHandler.readFromFile(file.getPath());
                armyNames.get(n).setText(army[n].getName());
                armyPaths.get(n).setText(file.getPath());
                unitLists.get(n).setItems((ObservableList<Unit>) army[n].getUnits());
                updateUnitInfo(n);
            } catch (UnsupportedFileExtensionException | CorruptedFileException | IOException e) {
                ErrorWindow.giveError(e.getMessage());
            } catch (NullPointerException e) {
                ErrorWindow.giveError("No file imported");
            }
        }
    }


    /**
     * method used to initialize scene
     * @param url the url
     * @param resourceBundle the resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        Winner.setVisible(false);
        reloadButton.setDisable(true);
        speed.getItems().setAll(Speed.values());
        terrain.getItems().setAll(Terrain.values());
        terrain.getSelectionModel().selectedItemProperty().addListener((ov, t, t1) -> {
            switch (t1){
                case FOREST -> background.setId("forest");
                case HILL -> background.setId("hill");
                case PLAINS -> background.setId("plains");
                case DESERT -> background.setId("desert");
                case VOLCANO -> background.setId("volcano");
                case ICEBERG -> background.setId("iceberg");
            }
        });
    }
}
