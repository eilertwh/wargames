package edu.ntnu.idatt2001.eilertwh.model.army;

import edu.ntnu.idatt2001.eilertwh.model.units.Unit;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.*;
import javafx.collections.FXCollections;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * class representing army with different types of units
 * @author Eilert Werner Hansen
 */
public class Army {

    private final String name;
    private final List<Unit> units;

    /**
     * Instantiates a new Army.
     *
     * @param name  the name
     * @param units the units
     * @throws IllegalArgumentException if name is blank or contains comma
     */
    public Army(String name, List<Unit> units) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name cannot be blank");
        if (name.contains(",")) throw new IllegalArgumentException("Name cannot contain comma");
        this.name = name;
        this.units = FXCollections.observableList(units);
    }

    /**
     * Instantiates a new Army.
     *
     * @param name the name
     */
    public Army(String name) {
        this(name, new ArrayList<>());
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets units.
     *
     * @return list with all units
     */
    public List<Unit> getUnits() {
        return units;
    }


    /**
     * Get infantry units.
     *
     * @return list with only the Infantry units
     */
    public List<Unit> getInfantryUnits(){
        return units.stream().filter(u -> u instanceof InfantryUnit).toList();
    }

    /**
     * Get cavalry units.
     *
     * @return list with only the Cavalry units excluding commander unit
     */
    public List<Unit> getCavalryUnits(){
        return units.stream().filter(u -> u instanceof CavalryUnit && !(u instanceof CommanderUnit)).toList();
    }

    /**
     * Get ranged units
     *
     * @return list with only the Ranged units
     */
    public List<Unit> getRangedUnits(){
        return units.stream().filter(u -> u instanceof RangedUnit).toList();
    }

    /**
     * Get commander unit list.
     *
     * @return list with only the Commander units
     */
    public List<Unit> getCommanderUnits (){
        return units.stream().filter(u -> u instanceof CommanderUnit ).toList();
    }

    /**
     * Get assassin unit list.
     *
     * @return list with only the Assassin units
     */
    public List<Unit> getAssassinUnit(){
        return units.stream().filter(u -> u instanceof AssassinUnit).toList();
    }

    /**
     * Get polar bear unit list.
     *
     * @return list with only the Polar Bear Units
     */
    public List<Unit> getPolarBearUnit(){
        return units.stream().filter(u -> u instanceof PolarBearUnit).toList();
    }

    /**
     * Get spearman unit list.
     *
     * @return list with only the Spearman units
     */
    public List<Unit> getSpearmanUnit(){
        return units.stream().filter(u -> u instanceof SpearmanUnit).toList();
    }

    /**
     * Add boolean.
     * adds a unit to the army
     *
     * @param unit the unit
     * @return true if successful, false if not
     */
    public boolean add(Unit unit) {
        if (units.contains(unit)) return false;
        return units.add(unit);
    }

    /**
     * Add all.
     * adds all units from an arraylist to the army
     *
     * @param units the units
     */
    public void addAll(List<Unit> units) {
        for (Unit unit : units) {
            this.add(unit);
        }
    }

    /**
     * Remove boolean.
     * removes a unit fom the army
     *
     * @param unit the unit
     * @return true if successful, false if not
     */
    public boolean remove(Unit unit) {
        return units.remove(unit);
    }

    /**
     * Has units boolean.
     *
     * @return true if the army has units, false if army does not have units
     */
    public boolean hasUnits() {
        return units.size() > 0;
    }

    /**
     * Gets random unit
     *
     * @return a random unit from the army
     */
    public Unit getRandom() {
        if (!this.hasUnits()) return null;
        Random r = new Random();
        int low = 0;
        int high = units.size();
        int random = r.nextInt(high - low) + low;
        return units.get(random);
    }

    /**
     * equals to see if to armies are the same
     * objects are the same if they have the same name, the same number of units, the same number
     * of infantry units,cavalry unit, ranged unit and commander unit
     *
     * @param o objects to be compared
     * @return true if they are the same false if not
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) &&
                getUnits().size() == army.getUnits().size() &&
                getInfantryUnits().size() == army.getInfantryUnits().size() &&
                getCavalryUnits().size() == army.getCavalryUnits().size() &&
                getRangedUnits().size() == army.getRangedUnits().size() &&
                getCommanderUnits().size() == army.getCommanderUnits().size();
    }

    /**
     * hashcode
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }

    /**
     * toString
     * @return String
     */
    @Override
    public String toString() {
        return ("Army{" + "name='" + name + '\'' + ", units=" + units.size() + '}');
    }
}
