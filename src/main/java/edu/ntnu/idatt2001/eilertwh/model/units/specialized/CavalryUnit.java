package edu.ntnu.idatt2001.eilertwh.model.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;

/**
 * The type Cavalry unit.
 * @author Eilert Werner Hansen
 */
public class CavalryUnit extends Unit {

    private int timesAttacked = 0;

    /**
     * Instantiates a new Cavalry unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     * @throws IllegalArgumentException the illegal argument exception if health is less than 0 or name is blank
     */
    public CavalryUnit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        super(name, health, attack, armor);
    }

    /**
     * gets unit type
     *
     * @return unit type
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.CAVALRY_UNIT;
    }

    /**
     * Instantiates a new Cavalry unit.
     *
     * @param name   the name
     * @param health the health
     * @throws IllegalArgumentException the illegal argument exception if health is less than 0 or name is blank
     */
    public CavalryUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, 20, 12);
    }

    /**
     * Gets attack bonus.
     *
     * @param terrain the terrain
     * @return the attack bonus of the unit
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int terrainBonus = 0;
        switch (terrain){
            case PLAINS -> terrainBonus = 2;
            case ICEBERG -> terrainBonus = -2;
        }
        timesAttacked++;
        if (timesAttacked == 1) return 4 + 2 + terrainBonus;
        return 2 + terrainBonus;
    }

    /**
     * Gets resist bonus.
     *
     * @param terrain the terrain
     * @return the resist bonus of the unit
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        int terrainBonus = 0;
        switch (terrain){
            case FOREST, VOLCANO -> terrainBonus = -1;
        }

        return 1 + terrainBonus;
    }
}
