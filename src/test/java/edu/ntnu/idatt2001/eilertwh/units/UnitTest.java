package edu.ntnu.idatt2001.eilertwh.units;

import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.CavalryUnit;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.InfantryUnit;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UnitTest {

    @Nested
    public class Unit_creation_test {

        @Test
        public void exception_thrown_if_unit_name_blank() {
            assertThrows(
                IllegalArgumentException.class,
                () -> new CavalryUnit("", 12)
            );
        }

        @Test
        public void exception_thrown_if_unit_name_contain_comma() {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> new CavalryUnit("hey, ho", 12)
            );
        }

        @Test
        public void exception_thrown_if_unit_health_is_negative() {
            assertThrows(
                IllegalArgumentException.class,
                () -> new CavalryUnit("Brage", -12)
            );
        }

        @Test
        public void exception_thrown_if_unit_attack_is_negative() {
            assertThrows(
                IllegalArgumentException.class,
                () -> new CavalryUnit("Brage", 12, -12, 12)
            );
        }

        @Test
        public void exception_thrown_if_unit_armor_is_negative() {
            assertThrows(
                IllegalArgumentException.class,
                () -> new CavalryUnit("Brage", 12, 12, -12)
            );
        }
    }


    @Nested
    public class Attack_test {

        @Test
        public void attack_test() {
            InfantryUnit attacker = (InfantryUnit) UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Carl", 10);
            InfantryUnit defender = (InfantryUnit) UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Brage", 15);
            attacker.attack(defender, Terrain.PLAINS);
            assertEquals(9, defender.getHealth());
            attacker.attack(defender, Terrain.PLAINS);
            assertEquals(3, defender.getHealth());
        }

        @Test
        public void attack_less_than_defence_attack_test() {
            InfantryUnit attacker = new InfantryUnit("Carl", 10, 5, 10);
            InfantryUnit defender = new InfantryUnit("Brage", 15, 5, 10);
            attacker.attack(defender, Terrain.PLAINS);
            assertEquals(15, defender.getHealth());
            attacker.attack(defender, Terrain.PLAINS);
            assertEquals(15, defender.getHealth());
            assertEquals(5,attacker.getAttack());
        }
    }
}
