package edu.ntnu.idatt2001.eilertwh.model.unitfactory;

import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.*;


import java.util.ArrayList;
import java.util.List;

/**
 * Utility class for creation of units
 *
 * @author Eilert Werner Hansen
 */
public class UnitFactory {

    /**
     * private constructor
     */
    private UnitFactory(){
        throw new UnsupportedOperationException();
    }

    /**
     * static method for creation of units
     *
     * @param unitType the unit type
     * @param name     the name of the unit
     * @param health   the health of the unit
     * @return created unit
     * @throws IllegalArgumentException illegal argument exception if unit type is not supported
     */
    public static Unit createUnit(UnitType unitType,String name,int health) throws IllegalArgumentException{
        switch (unitType){
            case INFANTRY_UNIT -> {return new InfantryUnit(name, health);}
            case RANGED_UNIT ->  {return new RangedUnit(name, health);}
            case CAVALRY_UNIT -> {return new CavalryUnit(name, health);}
            case COMMANDER_UNIT -> {return new CommanderUnit(name, health);}
            case ASSASSIN_UNIT -> {return new AssassinUnit(name, health);}
            case POLAR_BEAR_UNIT -> {return new PolarBearUnit(name, health);}
            case SPEARMAN_UNIT -> {return new SpearmanUnit(name, health);}
            default ->  throw new IllegalArgumentException("Unit type is not supported");
        }
    }

    /**
     * static method for creation of multiple units
     *
     * @param amount   the amount of units
     * @param unitType the unit type
     * @param name     the name of the units
     * @param health   the health of the units
     * @return list with created units
     */
    public static List<Unit> createUnits(int amount, UnitType unitType, String name, int health){
        ArrayList<Unit> units = new ArrayList<>();
        for (int i = 0; i <amount; i++) {
            units.add(createUnit(unitType, name, health));
        }
        return units;
    }
}
