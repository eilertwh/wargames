package edu.ntnu.idatt2001.eilertwh.model.battle;

import edu.ntnu.idatt2001.eilertwh.model.army.Army;
import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.*;
import javafx.collections.FXCollections;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * class used to simulate battle between armies.
 * @author Eilert Werner Hansen
 */
public class Battle {

    private final Army armyOne;

    private final Army armyTwo;

    private final Terrain terrain;

    private final List<String> battleLog;

    private Army winner;

    private boolean finished;

    /**
     * Instantiates a new Battle.
     *
     * @param armyOne army one
     * @param armyTwo army two
     * @param terrain the terrain
     * @throws IllegalArgumentException if the armies are the same, or an army is empty.
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain) throws IllegalArgumentException {
        if (armyOne.equals(armyTwo)) throw new IllegalArgumentException("Armies cannot be the same");
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()) throw new IllegalArgumentException("Armies must have units");
        if (terrain==null) throw new NullPointerException("Terrain cannot be null");
        if (armyOne == null || armyTwo == null)throw  new NullPointerException("Armies cannot be null");
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
        this.battleLog = FXCollections.observableList(new ArrayList<>());
        this.winner = null;
        this.finished = false;
    }

    /**
     * Simulates a single round in a battle
     *
     * @throws UnsupportedOperationException the unsupported operation exception
     */
    public void singleSimulate() throws UnsupportedOperationException {
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()) throw new UnsupportedOperationException(
                "Simulation can only be done once"
        );
        Random random = new Random();

        Army attackerArmy;
        Army defenderArmy;

        if (random.nextBoolean()) {
            attackerArmy = armyOne;
            defenderArmy = armyTwo;
        } else {
            attackerArmy = armyTwo;
            defenderArmy = armyOne;
        }

        Unit attackerUnit = attackerArmy.getRandom();
        Unit defenderUnit = defenderArmy.getRandom();
        int damage = attackerUnit.attack(defenderUnit,terrain);

        attackLog(attackerArmy,attackerUnit,defenderArmy,defenderUnit,damage);


        if (defenderUnit.getHealth() <= 0){
            defenderArmy.remove(defenderUnit);
            deathLog(defenderArmy, defenderUnit);
        }

        if (attackerUnit.getHealth() <= 0){
            attackerArmy.remove(attackerUnit);
            deathLog(attackerArmy, attackerUnit);
        }

        if (!defenderArmy.hasUnits()) {
            winner = attackerArmy;
            finished = true;
        }

        if (!attackerArmy.hasUnits()) {
            winner = attackerArmy;
            finished = true;
        }
    }
    /**
     * Simulate army.
     * method used to simulate a battle between to armies
     *
     * @throws UnsupportedOperationException if method is used more than once
     */
    public void simulate() throws UnsupportedOperationException {
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()) throw new UnsupportedOperationException(
            "Simulation can only be done once"
        );
        while (winner == null) singleSimulate();
    }

    /**
     * Gets battle log.
     *
     * @return the battle log
     */
    public List<String> getBattleLog() {
        return battleLog;
    }

    /**
     * method for logging attacks in simulation
     * @param attackerArmy the attacker Army
     * @param attackerUnit the attacker Unit
     * @param defenderArmy the defender army
     * @param defenderUnit the defender unit
     * @param damage the damage done
     */
    private void attackLog(Army attackerArmy, Unit attackerUnit, Army defenderArmy, Unit defenderUnit, int damage){
        Random random = new Random();

        String [] attackMessage = {};
        if (attackerUnit instanceof InfantryUnit) attackMessage = new String[]{" attacked ⚔ ", " attack ⚔ "};
        if (attackerUnit instanceof RangedUnit) attackMessage = new String[]{" shot \uD83C\uDFF9 ", " shoot \uD83C\uDFF9 "};
        if (attackerUnit instanceof CavalryUnit) attackMessage = new String[]{" trampled \uD83C\uDFC7 ", " trample \uD83C\uDFC7 "};
        if (attackerUnit instanceof AssassinUnit) attackMessage = new String[]{" backstabbed \uD83D\uDDE1 ", " backstab \uD83D\uDDE1 "};
        if (attackerUnit instanceof PolarBearUnit) attackMessage = new String[]{" bit \uD83D\uDC3B ", " eat \uD83D\uDC3B "};
        if (attackerUnit instanceof SpearmanUnit) attackMessage = new String[]{" threw spear at → ", " punch \uD83D\uDC4A "};

        String [] attackAdverb = {"",
                " brutally",
                " accidentally",
                " furiously",
                " joyfully",
                " elegantly",
                " savagely"};

        if (damage<0){
            battleLog.add(attackerArmy.getName() + "'s " + attackerUnit.getClass().getSimpleName() + " " + attackerUnit.getName()
                    + " tried to" +attackAdverb[random.nextInt(attackAdverb.length)] + attackMessage[1]
                    + defenderArmy.getName() + "'s " + defenderUnit.getClass().getSimpleName() + " " + defenderUnit.getName() + " but did " + (-damage) + " damage to themself." );
            return;
        }

        battleLog.add(attackerArmy.getName() + "'s " + attackerUnit.getClass().getSimpleName() + " " + attackerUnit.getName()
                + attackAdverb[random.nextInt(attackAdverb.length)] +  attackMessage[0]
                + defenderArmy.getName() + "'s " + defenderUnit.getClass().getSimpleName() + " " + defenderUnit.getName() + " and did " + (damage) + " damage." );
    }

    /**
     * method to log deaths
     *
     * @param defenderArmy the defender army
     * @param defenderUnit the defender unit
     */
    private void deathLog(Army defenderArmy,Unit defenderUnit){
        Random random = new Random();

        String [] deathMessage = {" died ☠",
                " died of their injuries \uD83E\uDD15",
                " bled to death \uD83D\uDCA7",
                " died of a broken heart \uD83D\uDC94",
                " exploded \uD83D\uDCA5",
                " died of death ☠",
                " died lol \uD83D\uDE02",
                " \"fell asleep\" \uD83D\uDCA4",
        };

        battleLog.add( defenderArmy.getName() + "'s " + defenderUnit.getClass().getSimpleName()
                + " " + defenderUnit.getName() + deathMessage[random.nextInt(deathMessage.length)]);
    }



    /**
     * Is finished boolean.
     *
     * @return true if finished false, if not
     */
    public boolean isFinished() {
        return finished;
    }

    /**
     * Gets winner.
     *
     * @return the winner
     */
    public Army getWinner() {
        return winner;
    }

    /**
     * toString
     * @return String
     */
    @Override
    public String toString() {
        return "Battle{" + "armyOne=" + armyOne + ", armyTwo=" + armyTwo + '}';
    }
}
