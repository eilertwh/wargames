package edu.ntnu.idatt2001.eilertwh;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.army.Army;
import edu.ntnu.idatt2001.eilertwh.model.battle.Battle;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;
import org.junit.jupiter.api.*;


@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class BattleTest {

    List<Unit> testUnitsOne;
    List<Unit> testUnitsTwo;

    @BeforeEach
    public void initiateArmies() {

        testUnitsOne = new ArrayList<>();
        testUnitsTwo = new ArrayList<>();

        testUnitsOne.addAll(UnitFactory.createUnits(500, UnitType.INFANTRY_UNIT,"Accensus", 20));
        testUnitsTwo.addAll(UnitFactory.createUnits(500, UnitType.INFANTRY_UNIT,"Accensus", 20));

        testUnitsOne.addAll(UnitFactory.createUnits(200, UnitType.RANGED_UNIT,"Sagittarii ", 15));
        testUnitsTwo.addAll(UnitFactory.createUnits(200, UnitType.RANGED_UNIT,"Sagittarii ", 15));

        testUnitsOne.addAll(UnitFactory.createUnits(300, UnitType.CAVALRY_UNIT,"Equites", 23));
        testUnitsTwo.addAll(UnitFactory.createUnits(300, UnitType.CAVALRY_UNIT,"Equites", 23));

        testUnitsOne.addAll(UnitFactory.createUnits(300, UnitType.ASSASSIN_UNIT,"hasisi", 10));
        testUnitsTwo.addAll(UnitFactory.createUnits(300, UnitType.ASSASSIN_UNIT,"hasisi", 10));

        testUnitsOne.addAll(UnitFactory.createUnits(300, UnitType.POLAR_BEAR_UNIT,"Knut", 25));
        testUnitsTwo.addAll(UnitFactory.createUnits(300, UnitType.POLAR_BEAR_UNIT,"Knut", 25));

        testUnitsOne.addAll(UnitFactory.createUnits(300, UnitType.SPEARMAN_UNIT,"pilum", 12));
        testUnitsTwo.addAll(UnitFactory.createUnits(300, UnitType.SPEARMAN_UNIT,"pilum", 12));

        testUnitsOne.add(UnitFactory.createUnit(UnitType.COMMANDER_UNIT,"Pompey", 23));
        testUnitsTwo.add(UnitFactory.createUnit(UnitType.COMMANDER_UNIT,"Julius Caesar", 30));
    }

    @Nested
    public class simulate_test {

        @Test
        public void winner_has_units_test() {
            Army armyOne = new Army("Caesars' army", testUnitsOne);
            Army armyTwo = new Army("Pompey's army", testUnitsTwo);
            Battle battle = new Battle(armyOne, armyTwo, Terrain.PLAINS);
            try {
                battle.simulate();
            } catch (UnsupportedOperationException e) {
                fail();
            }
            assertTrue(battle.getWinner().hasUnits());
        }

        @Test
        public void looser_has_no_units_test() {
            Army armyOne = new Army("Caesars' army", testUnitsOne);
            Army armyTwo = new Army("Pompey's army", testUnitsTwo);
            Battle battle = new Battle(armyOne, armyTwo, Terrain.PLAINS);
            try {
                battle.simulate();
            } catch (UnsupportedOperationException e) {
                fail();
            }
            if (battle.getWinner().equals(armyOne)) {
                assertFalse(armyTwo.hasUnits());
            } else {
                assertFalse(armyOne.hasUnits());
            }
        }

        @Test

        public void spearman_unit_soft_lock_test(){
            testUnitsOne = UnitFactory.createUnits(4, UnitType.SPEARMAN_UNIT,"pilum", 51);
            testUnitsTwo = UnitFactory.createUnits(4, UnitType.SPEARMAN_UNIT,"pilum", 51);
            Army armyOne = new Army("Caesars' army", testUnitsOne);
            Army armyTwo = new Army("Pompey's army", testUnitsTwo);
            Battle battle = new Battle(armyOne, armyTwo, Terrain.PLAINS);
            try {
                for (int i = 0; i <1000; i++) battle.singleSimulate();
                fail();
            } catch (UnsupportedOperationException e){
                assertTrue(battle.isFinished());
            }


        }
    }

    @Nested
    public class Log_tests{

        @Test
        public void log_empty_before_simulation(){
            Army armyOne = new Army("Caesars' army", testUnitsOne);
            Army armyTwo = new Army("Pompey's army", testUnitsTwo);
            Battle battle = new Battle(armyOne, armyTwo, Terrain.PLAINS);
            assertEquals(0,battle.getBattleLog().size());
        }

        @Test
        public void log_filled_after_simulation(){
            Army armyOne = new Army("Caesars' army", testUnitsOne);
            Army armyTwo = new Army("Pompey's army", testUnitsTwo);
            Battle battle = new Battle(armyOne, armyTwo, Terrain.PLAINS);
            try {
                battle.simulate();
            } catch (UnsupportedOperationException e) {
                fail();
            }
            assertNotEquals(0,battle.getBattleLog().size());
        }
    }

    @Nested
    public class Exception_tests {

        @Test
        public void duplicate_armies_exception_test() {
            assertThrows(
                IllegalArgumentException.class,
                () -> new Battle(new Army("Brage's army", testUnitsOne), new Army("Brage's army", testUnitsOne), Terrain.PLAINS)
            );
        }

        @Test
        public void empty_army_exception_test() {
            assertThrows(
                IllegalArgumentException.class,
                () -> new Battle(new Army("Brage's army", testUnitsOne), new Army("Carl's army"), Terrain.PLAINS)
            );
        }

        @Test
        public void second_simulation_exception_test() {
            Army armyOne = new Army("Caesars' army", testUnitsOne);
            Army armyTwo = new Army("Pompey's army", testUnitsTwo);
            Battle battle = new Battle(armyOne, armyTwo, Terrain.PLAINS);
            try {
                battle.simulate();
            } catch (UnsupportedOperationException e) {
                fail();
            }
            assertThrows(UnsupportedOperationException.class, battle::simulate);
        }
    }
}
