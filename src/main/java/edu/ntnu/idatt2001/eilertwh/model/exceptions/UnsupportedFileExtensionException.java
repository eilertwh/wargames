package edu.ntnu.idatt2001.eilertwh.model.exceptions;

/**
 * The type Unsupported file extension exception.
 * @author Eilert Werner Hansen
 */
public class UnsupportedFileExtensionException extends Exception{
    /**
     * Instantiates a new Unsupported file extension exception.
     *
     * @param message the message
     */
    public UnsupportedFileExtensionException(String message){
        super(message);
    }
}
