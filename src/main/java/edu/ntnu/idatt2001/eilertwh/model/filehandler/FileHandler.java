package edu.ntnu.idatt2001.eilertwh.model.filehandler;


import edu.ntnu.idatt2001.eilertwh.model.army.Army;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.exceptions.CorruptedFileException;
import edu.ntnu.idatt2001.eilertwh.model.exceptions.UnsupportedFileExtensionException;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;

import java.io.*;

/**
 * Utility class for writing and reading armies form files
 * @author Eilert Werner Hansen
 */
public class FileHandler  {

    /**
     * private constructor
     */
    private FileHandler() {
        throw new UnsupportedOperationException();
    }

    /**
     * formats unit to comma seperated values
     * @param unit to get formatted
     * @return sting in csv format
     */
    private static String commaSeparatedValues(Unit unit){
        return "\n" + unit.getClass().getSimpleName() + "," + unit.getName() + "," + unit.getHealth();
    }

    /**
     * writes army in a csv file in sorted order
     *
     * @param army the army to be written in the csv file
     * @param path the path of the file
     * @throws IOException              the io exception
     * @throws UnsupportedFileExtensionException if file extension of file is not csv
     */
    public static void sortedWriteToFile(Army army, String path) throws IOException, UnsupportedFileExtensionException, NullPointerException {
        if (army == null) throw new NullPointerException("Army cannot be null");
        if (!path.endsWith(".csv")) throw new UnsupportedFileExtensionException("File must have csv file extension");

        try (FileWriter fw = new FileWriter(path)){
            fw.write(army.getName());

            for (Unit unit: army.getInfantryUnits()) fw.write(commaSeparatedValues(unit));

            for (Unit unit: army.getCavalryUnits()) fw.write(commaSeparatedValues(unit));

            for (Unit unit: army.getRangedUnits()) fw.write(commaSeparatedValues(unit));

            for (Unit unit: army.getCommanderUnits()) fw.write(commaSeparatedValues(unit));

            for (Unit unit: army.getAssassinUnit()) fw.write(commaSeparatedValues(unit));

            for (Unit unit: army.getPolarBearUnit()) fw.write(commaSeparatedValues(unit));

            for (Unit unit: army.getSpearmanUnit()) fw.write(commaSeparatedValues(unit));
        }
    }

    /**
     * Write army to csv file
     *
     * @param army the army to be written in the csv file
     * @param path the path of the file
     * @throws IOException              the io exception
     * @throws UnsupportedFileExtensionException if file extension is not csv
     */
    public static void writeToFile(Army army, String path) throws IOException, UnsupportedFileExtensionException, NullPointerException {
        if (army == null) throw new NullPointerException("Army cannot be null");
        if (!path.endsWith(".csv")) throw new UnsupportedFileExtensionException("File must have csv file extension");

        try (FileWriter fw = new FileWriter(path)){

            fw.write(army.getName());

            for (Unit unit: army.getUnits()) fw.write(commaSeparatedValues(unit));
        }
    }

    /**
     * Reads csv file and turns returns the army.
     *
     * @param path the path of the csv file
     * @return the army created from the csv file
     * @throws UnsupportedFileExtensionException if file extension is not csv
     * @throws IOException              the io exception
     */
    public static Army readFromFile(String path) throws UnsupportedFileExtensionException, IOException, CorruptedFileException {
        if (!path.endsWith(".csv")) throw new UnsupportedFileExtensionException("File must have csv file extension");

        try(BufferedReader reader = new BufferedReader(new FileReader(path))) {
            //Checks if the file contains an army name
            String currentLine = reader.readLine();
            if (currentLine == null || currentLine.split(",").length !=1) throw new CorruptedFileException("Army name not found");
            //creates Army with the army name
            Army army = new Army(currentLine);

            while ((currentLine = reader.readLine()) != null) {
                String[] unitAttributes;
                unitAttributes = currentLine.split(",");
                if (unitAttributes.length != 3 ) throw new CorruptedFileException("file contains corrupted Units");
                // checks if Unit health is Corrupted
                int health;
                String name = unitAttributes[1];

                try {
                    health = Integer.parseInt(unitAttributes[2]);
                } catch (NumberFormatException e){
                    throw new CorruptedFileException("file contains units with corrupted Health");
                }

                if (health <= 0) throw new CorruptedFileException("file contains units with no or negative health");

                //Adds units into army
                switch (unitAttributes[0]) {
                    case "InfantryUnit" -> army.add(UnitFactory.createUnit(UnitType.INFANTRY_UNIT,name,health));
                    case "RangedUnit" -> army.add(UnitFactory.createUnit(UnitType.RANGED_UNIT,name,health));
                    case "CavalryUnit" -> army.add(UnitFactory.createUnit(UnitType.CAVALRY_UNIT,name,health));
                    case "CommanderUnit" -> army.add(UnitFactory.createUnit(UnitType.COMMANDER_UNIT,name,health));
                    case "AssassinUnit" -> army.add(UnitFactory.createUnit(UnitType.ASSASSIN_UNIT,name,health));
                    case "PolarBearUnit" -> army.add(UnitFactory.createUnit(UnitType.POLAR_BEAR_UNIT,name,health));
                    case "SpearmanUnit" -> army.add(UnitFactory.createUnit(UnitType.SPEARMAN_UNIT,name,health));
                    default -> throw new CorruptedFileException("file contains unsupported units");
                }
            }
            return army;
        }
    }

}
