package edu.ntnu.idatt2001.eilertwh.model.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;

/**
 * The type Ranged unit.
 * @author Eilert Werner Hansen
 */
public class RangedUnit extends Unit {

    private int timesResisted = 0;

    /**
     * Instantiates a new Ranged unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 or name is blank
     */
    public RangedUnit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        super(name, health, attack, armor);
    }

    /**
     * gets unit type
     *
     * @return unit type
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.RANGED_UNIT;
    }

    /**
     * Instantiates a new Ranged unit.
     *
     * @param name   the name
     * @param health the health
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 or name is blank
     */
    public RangedUnit(String name, int health) throws IllegalArgumentException{
        super(name, health, 15, 8);
    }

    /**
     * Gets attack bonus.
     *
     * @param terrain the terrain
     * @return the attack bonus of the unit
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int terrainBonus = 0;
        switch (terrain){
            case HILL, ICEBERG -> terrainBonus = 2;
            case FOREST -> terrainBonus = -2;
        }

        return 3 + terrainBonus;
    }

    /**
     * Gets resist bonus.
     *
     * @param terrain the terrain
     * @return the resist bonus of the unit
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        int terrainBonus = 0;
        timesResisted++;
        if (terrain == Terrain.VOLCANO) terrainBonus = -2;


        if (timesResisted == 1) return 6 + terrainBonus;
        if (timesResisted == 2) return 4 + terrainBonus;
        return 2 + terrainBonus;
    }
}
