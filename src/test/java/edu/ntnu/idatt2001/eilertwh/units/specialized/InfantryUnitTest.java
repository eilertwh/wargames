package edu.ntnu.idatt2001.eilertwh.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.InfantryUnit;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class InfantryUnitTest {

    @Nested
    public class Get_bonus_test {

        @Test
        public void infantry_unit_get_attack_bonus() {
            InfantryUnit infantryUnit = (InfantryUnit) UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Brage", 10);
            assertEquals(2, infantryUnit.getAttackBonus(Terrain.PLAINS));
        }

        @Test
        public void forest_infantry_unit_get_attack_bonus() {
            InfantryUnit infantryUnit = (InfantryUnit) UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Brage", 10);
            assertEquals(4, infantryUnit.getAttackBonus(Terrain.FOREST));
        }


        @Test
        public void infantry_unit_get_resist_bonus() {
            InfantryUnit infantryUnit = (InfantryUnit) UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Brage", 10);
            assertEquals(1, infantryUnit.getResistBonus(Terrain.PLAINS));
        }

        @Test
        public void forest_infantry_unit_get_resist_bonus() {
            InfantryUnit infantryUnit = (InfantryUnit) UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Brage", 10);
            assertEquals(3, infantryUnit.getResistBonus(Terrain.FOREST));
        }

        @Test
        public void volcano_infantry_unit_get_resist_bonus() {
            InfantryUnit infantryUnit = (InfantryUnit) UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Brage", 10);
            assertEquals(0, infantryUnit.getResistBonus(Terrain.VOLCANO));
        }
    }
}
