package edu.ntnu.idatt2001.eilertwh.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.CavalryUnit;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class CavalryUnitTest {

    @Nested
    public class Get_bonus_test {

        @Test
        public void cavalry_unit_get_attack_bonus() {
            CavalryUnit cavalryUnit = (CavalryUnit) UnitFactory.createUnit(UnitType.CAVALRY_UNIT,"Brage", 10);
            assertEquals(6, cavalryUnit.getAttackBonus(Terrain.FOREST));
            assertEquals(2, cavalryUnit.getAttackBonus(Terrain.FOREST));
            assertEquals(2, cavalryUnit.getAttackBonus(Terrain.FOREST));
        }

        @Test
        public void plains_cavalry_unit_get_attack_bonus() {
            CavalryUnit cavalryUnit = (CavalryUnit) UnitFactory.createUnit(UnitType.CAVALRY_UNIT,"Brage", 10);
            assertEquals(8, cavalryUnit.getAttackBonus(Terrain.PLAINS));
            assertEquals(4, cavalryUnit.getAttackBonus(Terrain.PLAINS));
            assertEquals(4, cavalryUnit.getAttackBonus(Terrain.PLAINS));
        }

        @Test
        public void iceberg_cavalry_unit_get_attack_bonus() {
            CavalryUnit cavalryUnit = (CavalryUnit) UnitFactory.createUnit(UnitType.CAVALRY_UNIT,"Brage", 10);
            assertEquals(4, cavalryUnit.getAttackBonus(Terrain.ICEBERG));
            assertEquals(0, cavalryUnit.getAttackBonus(Terrain.ICEBERG));
            assertEquals(0, cavalryUnit.getAttackBonus(Terrain.ICEBERG));
        }


        @Test
        public void cavalry_unit_get_resist_bonus() {
            CavalryUnit cavalryUnit = (CavalryUnit) UnitFactory.createUnit(UnitType.CAVALRY_UNIT,"Brage", 10);
            assertEquals(1, cavalryUnit.getResistBonus(Terrain.PLAINS));
        }

        @Test
        public void forest_cavalry_unit_get_resist_bonus() {
            CavalryUnit cavalryUnit = (CavalryUnit) UnitFactory.createUnit(UnitType.CAVALRY_UNIT,"Brage", 10);
            assertEquals(0, cavalryUnit.getResistBonus(Terrain.FOREST));
        }

        @Test
        public void volcano_cavalry_unit_get_resist_bonus() {
            CavalryUnit cavalryUnit = (CavalryUnit) UnitFactory.createUnit(UnitType.CAVALRY_UNIT,"Brage", 10);
            assertEquals(0, cavalryUnit.getResistBonus(Terrain.VOLCANO));
        }
    }
}
