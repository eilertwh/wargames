package edu.ntnu.idatt2001.eilertwh.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.CommanderUnit;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class CommanderUnitTest {


    @Nested
    public class Get_bonus_test {

        @Test
        public void commander_unit_get_attack_bonus() {
            CommanderUnit commanderUnit = (CommanderUnit) UnitFactory.createUnit(UnitType.COMMANDER_UNIT,"Brage", 10);
            assertEquals(6, commanderUnit.getAttackBonus(Terrain.FOREST));
            assertEquals(2, commanderUnit.getAttackBonus(Terrain.FOREST));
            assertEquals(2, commanderUnit.getAttackBonus(Terrain.FOREST));
        }

        @Test
        public void plains_commander_unit_get_attack_bonus() {
            CommanderUnit commanderUnit = (CommanderUnit) UnitFactory.createUnit(UnitType.COMMANDER_UNIT,"Brage", 10);
            assertEquals(8, commanderUnit.getAttackBonus(Terrain.PLAINS));
            assertEquals(4, commanderUnit.getAttackBonus(Terrain.PLAINS));
            assertEquals(4, commanderUnit.getAttackBonus(Terrain.PLAINS));
        }

        @Test
        public void iceberg_commander_unit_get_attack_bonus() {
            CommanderUnit commanderUnit = (CommanderUnit) UnitFactory.createUnit(UnitType.COMMANDER_UNIT,"Brage", 10);
            assertEquals(4, commanderUnit.getAttackBonus(Terrain.ICEBERG));
            assertEquals(0, commanderUnit.getAttackBonus(Terrain.ICEBERG));
            assertEquals(0, commanderUnit.getAttackBonus(Terrain.ICEBERG));
        }


        @Test
        public void commander_unit_get_resist_bonus() {
            CommanderUnit commanderUnit = (CommanderUnit) UnitFactory.createUnit(UnitType.COMMANDER_UNIT,"Brage", 10);
            assertEquals(1, commanderUnit.getResistBonus(Terrain.PLAINS));
        }

        @Test
        public void forest_commander_unit_get_resist_bonus() {
            CommanderUnit commanderUnit = (CommanderUnit) UnitFactory.createUnit(UnitType.COMMANDER_UNIT,"Brage", 10);
            assertEquals(0, commanderUnit.getResistBonus(Terrain.FOREST));
        }

        @Test
        public void volcano_commander_unit_get_resist_bonus() {
            CommanderUnit commanderUnit = (CommanderUnit) UnitFactory.createUnit(UnitType.COMMANDER_UNIT,"Brage", 10);
            assertEquals(0, commanderUnit.getResistBonus(Terrain.VOLCANO));
        }
    }
}
