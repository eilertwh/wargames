package edu.ntnu.idatt2001.eilertwh.client;

import javafx.scene.control.Alert;

/**
 * gives error to user
 * @author Eilert Werner Hansen
 */
public class ErrorWindow {
    /**
     * Give error.
     * method copied from idatt1002 project
     * @param message the message to be shown
     */
    public static void giveError(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.showAndWait();
    }
}
