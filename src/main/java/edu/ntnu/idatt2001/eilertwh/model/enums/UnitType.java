package edu.ntnu.idatt2001.eilertwh.model.enums;


/**
 * enum representing Unit type.
 * @author Eilert Werner Hansen
 */
public enum UnitType {
    /**
     * Infantry unit type.
     */
    INFANTRY_UNIT,
    /**
     * Ranged unit type.
     */
    RANGED_UNIT,
    /**
     * Cavalry unit type.
     */
    CAVALRY_UNIT,
    /**
     * Commander unit type.
     */
    COMMANDER_UNIT,
    /**
     * Assassin unit type.
     */
    ASSASSIN_UNIT,
    /**
     * Polar bear unit type.
     */
    POLAR_BEAR_UNIT,
    /**
     * Spearman unit type.
     */
    SPEARMAN_UNIT;


    /**
     * ToString
     * @return toString
     */

    @Override
        public String toString() {
            return super.toString().replace("_"," ").replace("UNIT","");
        }
    }
