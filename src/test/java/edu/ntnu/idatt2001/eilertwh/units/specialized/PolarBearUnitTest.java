package edu.ntnu.idatt2001.eilertwh.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.PolarBearUnit;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class PolarBearUnitTest {

    @Nested
    public class Get_bonus_test {

        @Test
        public void polar_bear_unit_get_attack_bonus() {
            PolarBearUnit polarBearUnit = (PolarBearUnit) UnitFactory.createUnit(UnitType.POLAR_BEAR_UNIT, "Brage", 10);
            assertEquals(8, polarBearUnit.getAttackBonus(Terrain.PLAINS));
            assertEquals(2, polarBearUnit.getAttackBonus(Terrain.PLAINS));
            assertEquals(2, polarBearUnit.getAttackBonus(Terrain.PLAINS));
        }

        @Test
        public void iceberg_polar_bear_unit_get_attack_bonus() {
            PolarBearUnit polarBearUnit = (PolarBearUnit) UnitFactory.createUnit(UnitType.POLAR_BEAR_UNIT, "Brage", 10);
            assertEquals(10, polarBearUnit.getAttackBonus(Terrain.ICEBERG));
            assertEquals(4, polarBearUnit.getAttackBonus(Terrain.ICEBERG));
            assertEquals(4, polarBearUnit.getAttackBonus(Terrain.ICEBERG));
        }

        @Test
        public void polar_bear_unit_get_resist_bonus() {
            PolarBearUnit polarBearUnit = (PolarBearUnit) UnitFactory.createUnit(UnitType.POLAR_BEAR_UNIT, "Brage", 10);
            assertEquals(2, polarBearUnit.getResistBonus(Terrain.PLAINS));
            polarBearUnit.getAttackBonus(Terrain.PLAINS);
            assertEquals(6, polarBearUnit.getResistBonus(Terrain.PLAINS));
            polarBearUnit.getAttackBonus(Terrain.PLAINS);
            assertEquals(6, polarBearUnit.getResistBonus(Terrain.PLAINS));
        }

        @Test
        public void iceberg_polar_bear_unit_get_resist_bonus() {
            PolarBearUnit polarBearUnit = (PolarBearUnit) UnitFactory.createUnit(UnitType.POLAR_BEAR_UNIT, "Brage", 10);
            assertEquals(4, polarBearUnit.getResistBonus(Terrain.ICEBERG));
            polarBearUnit.getAttackBonus(Terrain.PLAINS);
            assertEquals(8, polarBearUnit.getResistBonus(Terrain.ICEBERG));
            polarBearUnit.getAttackBonus(Terrain.PLAINS);
            assertEquals(8, polarBearUnit.getResistBonus(Terrain.ICEBERG));
        }

        @Test
        public void desert_polar_bear_unit_get_resist_bonus() {
            PolarBearUnit polarBearUnit = (PolarBearUnit) UnitFactory.createUnit(UnitType.POLAR_BEAR_UNIT, "Brage", 10);
            assertEquals(0, polarBearUnit.getResistBonus(Terrain.DESERT));
            polarBearUnit.getAttackBonus(Terrain.PLAINS);
            assertEquals(4, polarBearUnit.getResistBonus(Terrain.DESERT));
            polarBearUnit.getAttackBonus(Terrain.PLAINS);
            assertEquals(4, polarBearUnit.getResistBonus(Terrain.DESERT));
        }

        @Test
        public void volcano_polar_bear_unit_get_resist_bonus() {
            PolarBearUnit polarBearUnit = (PolarBearUnit) UnitFactory.createUnit(UnitType.POLAR_BEAR_UNIT, "Brage", 10);
            assertEquals(0, polarBearUnit.getResistBonus(Terrain.VOLCANO));
            polarBearUnit.getAttackBonus(Terrain.PLAINS);
            assertEquals(4, polarBearUnit.getResistBonus(Terrain.VOLCANO));
            polarBearUnit.getAttackBonus(Terrain.PLAINS);
            assertEquals(4, polarBearUnit.getResistBonus(Terrain.VOLCANO));
        }
    }

}
