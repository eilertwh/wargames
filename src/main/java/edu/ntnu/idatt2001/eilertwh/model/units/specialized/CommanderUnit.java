package edu.ntnu.idatt2001.eilertwh.model.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;

/**
 * The type Commander unit.
 * @author Eilert Werner Hansen
 */
public class CommanderUnit extends CavalryUnit {

    /**
     * Instantiates a new Commander unit.
     *
     * @param name   the name
     * @param health the health
     * @param attack the attack
     * @param armor  the armor
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 or name is blank
     */
    public CommanderUnit(String name, int health, int attack, int armor) throws IllegalArgumentException {
        super(name, health, attack, armor);
    }

    /**
     * Instantiates a new Commander unit.
     *
     * @param name   the name
     * @param health the health
     * @throws IllegalArgumentException illegal argument exception if health is less than 0 or name is blank
     */
    public CommanderUnit(String name, int health) throws IllegalArgumentException {
        super(name, health, 25, 15);
    }

    /**
     * gets unit type
     *
     * @return unit type
     */
    @Override
    public UnitType getUnitType() {
        return UnitType.COMMANDER_UNIT;
    }
}
