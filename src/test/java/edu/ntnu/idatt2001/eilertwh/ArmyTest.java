package edu.ntnu.idatt2001.eilertwh;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.army.Army;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.*;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;
import org.junit.jupiter.api.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class ArmyTest {

    @Nested
    public class Add_and_remove_unit_test {

        @Test
        public void add_single_unit_test() {
            Army army = new Army("Elven army");
            InfantryUnit unit = (InfantryUnit) UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Brage", 10);
            assertTrue(army.add(unit));
            assertTrue(army.getUnits().contains(unit));
        }

        @Test
        public void add_duplicate_Unit_test() {
            Army army = new Army("Elven army");
            InfantryUnit unit = (InfantryUnit) UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Brage", 10);
            army.add(unit);
            assertFalse(army.add(unit));
            assertEquals(1, army.getUnits().size());
        }

        @Test
        public void add_multiple_units_test() {
            Army army = new Army("Humans");
            army.addAll(UnitFactory.createUnits(100, UnitType.INFANTRY_UNIT,"Brage", 34));
            assertEquals(100, army.getUnits().size());
        }

        @Test
        public void add_multiple_duplicate_units_test() {
            Army army = new Army("Humans");
            List<Unit> toAdd = UnitFactory.createUnits(100, UnitType.INFANTRY_UNIT,"Brage", 34);
            army.addAll(toAdd);
            army.addAll(toAdd);
            assertEquals(100, army.getUnits().size());
        }

        @Test
        public void remove_unit_test() {
            Army army = new Army("Humans");
            army.addAll(UnitFactory.createUnits(100, UnitType.INFANTRY_UNIT,"Brage", 34));
            InfantryUnit infantryUnit = new InfantryUnit("brage", 34);
            army.add(infantryUnit);
            assertTrue(army.getUnits().contains(infantryUnit));
            assertEquals(101, army.getUnits().size());
            army.remove(infantryUnit);
            assertFalse(army.getUnits().contains(infantryUnit));
            assertEquals(100, army.getUnits().size());
        }
    }

    @Nested
    public class Has_units_test {

        @Test
        public void has_units_test() {
            Army army = new Army("humans");
            army.add(UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"brage",69));
            assertTrue(army.hasUnits());
        }

        @Test
        public void has_no_unit_test() {
            Army army = new Army("humans");
            assertFalse(army.hasUnits());
        }
    }

    @Nested
    public class Get_random_unit_test {

        @Test
        public void no_units_test() {
            Army army = new Army("Human");
            assertNull(army.getRandom());
        }

        @Test
        public void get_unit_test() {
            Army army = new Army("Human",UnitFactory.createUnits(100,UnitType.INFANTRY_UNIT,"brage",32));
            assertNotNull(army.getRandom());
        }
    }

    @Nested
    public class Exception_tests {
        @Test
        public void blank_name_exception_test() {
            assertThrows(
                IllegalArgumentException.class,
                () -> new Army("")
            );
        }

        @Test
        public void name_with_comma_exception_test() {
            assertThrows(
                    IllegalArgumentException.class,
                    () -> new Army("hei, hallo")
            );
        }
    }

    @Nested
    public class get_Specialized_Unit_List_Tests {
        static Army army = new Army("Army");

        @BeforeAll
        private static void fillArmy(){
            army.addAll(UnitFactory.createUnits(100,UnitType.INFANTRY_UNIT,"Brage",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.CAVALRY_UNIT,"Carl",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.COMMANDER_UNIT,"Nicolai",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.RANGED_UNIT,"Callum",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.ASSASSIN_UNIT,"Runar",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.POLAR_BEAR_UNIT,"Trym",10));
            army.addAll(UnitFactory.createUnits(100,UnitType.SPEARMAN_UNIT,"Tomas",10));

        }

        @Test
        public void get_Infantry_units_test(){
            List<Unit> InfantryUnits = army.getInfantryUnits();

            assertEquals(100,InfantryUnits.size());
            assertFalse(InfantryUnits
                    .stream()
                    .anyMatch(u -> !(u instanceof InfantryUnit)));

        }

        @Test
        public void get_Cavalry_units_test(){
            List<Unit> cavalryUnits = army.getCavalryUnits();

            assertEquals(100,cavalryUnits.size());
            assertFalse(cavalryUnits
                    .stream()
                    .anyMatch(u ->  u instanceof CommanderUnit));

        }

        @Test
        public void get_ranged_units_test(){
            List<Unit> rangedUnits = army.getRangedUnits();

            assertEquals(100, rangedUnits.size());
            assertFalse(rangedUnits
                    .stream()
                    .anyMatch(u -> !(u instanceof RangedUnit)));

        }

        @Test
        public void get_commander_unit_test() {
            List<Unit> commanderUnits = army.getCommanderUnits();


            assertEquals(100, commanderUnits.size());
            assertFalse(commanderUnits
                    .stream()
                    .anyMatch(u -> !(u instanceof CommanderUnit)));
        }

        @Test
        public void get_assassin_unit_test() {
            List<Unit> assassinUnits = army.getAssassinUnit();


            assertEquals(100, assassinUnits.size());
            assertFalse(assassinUnits
                    .stream()
                    .anyMatch(u -> !(u instanceof AssassinUnit)));
        }

        @Test
        public void get_polar_bear_unit_test() {
            List<Unit> polarBearUnits = army.getPolarBearUnit();


            assertEquals(100, polarBearUnits.size());
            assertFalse(polarBearUnits
                    .stream()
                    .anyMatch(u -> !(u instanceof PolarBearUnit)));
        }

        @Test
        public void get_spearman_unit_test() {
            List<Unit> spearman = army.getSpearmanUnit();


            assertEquals(100, spearman.size());
            assertFalse(spearman
                    .stream()
                    .anyMatch(u -> !(u instanceof SpearmanUnit)));
        }
    }
}
