package edu.ntnu.idatt2001.eilertwh.model.enums;

/**
 * enum representing Terrain.
 */
public enum Terrain {
    /**
     * Hill terrain.
     */
    HILL,
    /**
     * Plains terrain.
     */
    PLAINS,
    /**
     * Forest terrain.
     */
    FOREST,
    /**
     * Desert terrain.
     */
    DESERT,
    /**
     * Volcano terrain.
     */
    VOLCANO,
    /**
     * Iceberg terrain.
     */
    ICEBERG
}
