package edu.ntnu.idatt2001.eilertwh.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.RangedUnit;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class RangedUnitTest {

    @Nested
    public class Get_bonus_test {

        @Test
        public void ranged_unit_get_resist_bonus() {
            RangedUnit rangedUnit = (RangedUnit) UnitFactory.createUnit(UnitType.RANGED_UNIT,"Brage", 10);
            assertEquals(6, rangedUnit.getResistBonus(Terrain.PLAINS));
            assertEquals(4, rangedUnit.getResistBonus(Terrain.PLAINS));
            assertEquals(2, rangedUnit.getResistBonus(Terrain.PLAINS));
            assertEquals(2, rangedUnit.getResistBonus(Terrain.PLAINS));
        }

        @Test
        public void volcano_ranged_unit_get_resist_bonus() {
            RangedUnit rangedUnit = (RangedUnit) UnitFactory.createUnit(UnitType.RANGED_UNIT,"Brage", 10);
            assertEquals(4, rangedUnit.getResistBonus(Terrain.VOLCANO));
            assertEquals(2, rangedUnit.getResistBonus(Terrain.VOLCANO));
            assertEquals(0, rangedUnit.getResistBonus(Terrain.VOLCANO));
            assertEquals(0, rangedUnit.getResistBonus(Terrain.VOLCANO));
        }

        @Test
        public void ranged_unit_get_attack_bonus() {
            RangedUnit rangedUnit =  (RangedUnit) UnitFactory.createUnit(UnitType.RANGED_UNIT,"Brage", 10);
            assertEquals(3, rangedUnit.getAttackBonus(Terrain.PLAINS));
        }

        @Test
        public void hill_unit_get_attack_bonus() {
            RangedUnit rangedUnit =  (RangedUnit) UnitFactory.createUnit(UnitType.RANGED_UNIT,"Brage", 10);
            assertEquals(5, rangedUnit.getAttackBonus(Terrain.HILL));
        }

        @Test
        public void iceberg_unit_get_attack_bonus() {
            RangedUnit rangedUnit =  (RangedUnit) UnitFactory.createUnit(UnitType.RANGED_UNIT,"Brage", 10);
            assertEquals(5, rangedUnit.getAttackBonus(Terrain.ICEBERG));
        }

        @Test
        public void forest_unit_get_attack_bonus() {
            RangedUnit rangedUnit =  (RangedUnit) UnitFactory.createUnit(UnitType.RANGED_UNIT,"Brage", 10);
            assertEquals(1, rangedUnit.getAttackBonus(Terrain.FOREST));
        }
    }
}
