package edu.ntnu.idatt2001.eilertwh.units.specialized;

import edu.ntnu.idatt2001.eilertwh.model.enums.Terrain;
import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.SpearmanUnit;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SpearmanUnitTest {
    @Nested
    public class Get_bonus_test {

        @Test
        public void spearman_unit_get_attack_bonus() {
            SpearmanUnit spearmanUnit = (SpearmanUnit) UnitFactory.createUnit(UnitType.SPEARMAN_UNIT, "Brage", 10);
            assertEquals(50, spearmanUnit.getAttackBonus(Terrain.PLAINS));
            assertEquals(0, spearmanUnit.getAttackBonus(Terrain.PLAINS));
            assertEquals(0, spearmanUnit.getAttackBonus(Terrain.PLAINS));
        }

        @Test
        public void hill_spearman_unit_get_attack_bonus() {
            SpearmanUnit spearmanUnit = (SpearmanUnit) UnitFactory.createUnit(UnitType.SPEARMAN_UNIT, "Brage", 10);
            assertEquals(54, spearmanUnit.getAttackBonus(Terrain.HILL));
            assertEquals(0, spearmanUnit.getAttackBonus(Terrain.HILL));
            assertEquals(0, spearmanUnit.getAttackBonus(Terrain.HILL));
        }

        @Test
        public void iceberg_spearman_unit_get_attack_bonus() {
            SpearmanUnit spearmanUnit = (SpearmanUnit) UnitFactory.createUnit(UnitType.SPEARMAN_UNIT, "Brage", 10);
            assertEquals(54, spearmanUnit.getAttackBonus(Terrain.ICEBERG));
            assertEquals(0, spearmanUnit.getAttackBonus(Terrain.ICEBERG));
            assertEquals(0, spearmanUnit.getAttackBonus(Terrain.ICEBERG));
        }

        @Test
        public void forest_spearman_unit_get_attack_bonus() {
            SpearmanUnit spearmanUnit = (SpearmanUnit) UnitFactory.createUnit(UnitType.SPEARMAN_UNIT, "Brage", 10);
            assertEquals(40, spearmanUnit.getAttackBonus(Terrain.FOREST));
            assertEquals(0, spearmanUnit.getAttackBonus(Terrain.FOREST));
            assertEquals(0, spearmanUnit.getAttackBonus(Terrain.FOREST));
        }

        @Test
        public void spearman_unit_get_resist_bonus() {
            SpearmanUnit spearmanUnit = (SpearmanUnit) UnitFactory.createUnit(UnitType.SPEARMAN_UNIT, "Brage", 10);
            assertEquals(1, spearmanUnit.getResistBonus(Terrain.PLAINS));
        }

        @Test
        public void volcano_spearman_unit_get_resist_bonus() {
            SpearmanUnit spearmanUnit = (SpearmanUnit) UnitFactory.createUnit(UnitType.SPEARMAN_UNIT, "Brage", 10);
            assertEquals(0, spearmanUnit.getResistBonus(Terrain.VOLCANO));
        }
    }
}
