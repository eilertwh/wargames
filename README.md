# ProsjektRapport

## Innledning

Denne rapporten er en del av prosjektet &quot;Wargames&quot; gitt i emnet IDATT2001 Programmering 2 på NTNU.
Målet med dette prosjektet var å lage et program med kodespråket Java for å simulere et slag mellom to armeer.
Som en del av kravene,ble JavaFX brukt til å opprette et grafisk brukergrensesnitt også kalt GUI.
Andre krav var å bruke kunnskapen om designprinsipper, filhåndtering, GUI-implementeringer, funksjonell programmering og implementere det i programmets kode.

## Kravspesifikasjon

Når man først kjører programmet kommer man til hovedsiden, den er tredelt. Vestre og høyre del tilhører hver sin arme.
Her ser man navn på arme, antall units totalt og av hver type, en liste med alle units og filsti til armenes filer.
Det er også knapper for å importere armer fra en CSV fil.
I midten av siden har man en knapp for å starte simuleringen, en knapp for å laste inn armer på nytt,
dropdown meny for å velge terreng til slaget, dropdown meny for å velge hastighet til slaget og en knapp for å lage en ny arme.
Når man velger terreng, forandrer bakgrunnen seg og ulike units får bonus til angrep og forsvar basert terrenget.
Simuleringen har forskjellige hastigheter: sakte, medium, raskt og øyeblikkelig.
Når simuleringen kjøres vil det dukke opp hvilke units som angriper hvilke units og når units dør i en logg.
Etter simulering vil vinneren vises over loggen.

Hvis man trykker på create army kommer et nytt vindu. Her kan man lage nye armer. Til høyre har man en liste med units.
I midten ser man antall units totalt og av hver type.
Til venstre er det felt for å lage eller fjerne units. Disse feltene er tekstfelt for unit navn,
dropdown meny for unit type, spinnere for helse og antall units.
Under feltene er det knapper for å legge til eller fjerne units basert på feltene over.
Hvis Unitname er tomt vil den legge til units med navn «unit» og navnene blir ignorert når den fjerner tropper.
Nederst på siden har man knapper for å importere en eksisterende arme og for å lagre armen til en fil.
Når man lagrer armen blir den skrevet til en CSV fil og armens navn blir det samme som filnavnet.
Det er også mulig å overskrive eksisterende filer.

## Design

Når det kommer til design er det hovedsakelig brukt design mønstrene: MVC, Fabrikk og Observer.
MVC ble brukt til å strukturere arkitekturen til programmet. Her er arkitekturen til Model delen av programmet
![](ReportPictures/ModelKlassediagram.png)

Fabrikker ble brukt til å enkelt lage nye Units. Observer ble brukt til å oppdatere listviews og bakgrunner i GUI.

Programmet ble designet med tanke på å ha løs kobling og høy kohesjon i koden.
For å få til dette ble det fulgt designprinsipp som SRS. Klassene holder seg til sine egne funksjoner.

Andre design prinsipp som ble brukt er: LSP, KISS, DRY.
LSP ble brukt i units, alle units er utskiftbare med hverandre når koden spør etter en unit.
DRY ble brukt gjennom hele koden, nye metoder ble laget istendenfor å skrive samme kode mange ganger.
KISS ble også brukt gjennom hele koden, hvis en metode har hatt flere bruks områder har den blitt splittet til flere metoder.

## Implementasjon

Maven ble satt opp med Group ID edu.ntnu.idatt2001.eilertwh og artifactId wargames. Java versjonen valg er Java 16.
Det er importert dependies for enhetstesting og JavaFX

Dette er pakke strukturen til programmet:

```
.
├── java
│   ├── edu
│   │   └── ntnu
│   │       └── idatt2001
│   │           └── eilertwh
│   │               ├── client
│   │               │   ├── App.java
│   │               │   ├── ErrorWindow.java
│   │               │   └── controller
│   │               │       ├── CreateArmyController.java
│   │               │       └── WargamesController.java
│   │               └── model
│   │                   ├── army
│   │                   │   └── Army.java
│   │                   ├── battle
│   │                   │   └── Battle.java
│   │                   ├── enums
│   │                   │   ├── Speed.java
│   │                   │   ├── Terrain.java
│   │                   │   └── UnitType.java
│   │                   ├── exceptions
│   │                   │   ├── CorruptedFileException.java
│   │                   │   └── UnsupportedFileExtensionException.java
│   │                   ├── filehandler
│   │                   │   └── FileHandler.java
│   │                   ├── unitfactory
│   │                   │   └── UnitFactory.java
│   │                   └── units
│   │                       ├── Unit.java
│   │                       └── specialized
│   │                           ├── AssassinUnit.java
│   │                           ├── CavalryUnit.java
│   │                           ├── CommanderUnit.java
│   │                           ├── InfantryUnit.java
│   │                           ├── PolarBearUnit.java
│   │                           ├── RangedUnit.java
│   │                           └── SpearmanUnit.java
│   └── module-info.java
└── resources
    ├── Castellar.ttf
    ├── Theme.css
    ├── View
    │   ├── CreateArmy.fxml
    │   └── Wargames.fxml
    ├── Wargames.png
    └── backgrounds
        ├── Create.jpg
        ├── Desert.jpg
        ├── Forest.jpg
        ├── Hill.jpg
        ├── Iceberg.jpg
        ├── MainPage.png
        ├── Plains.png
        └── Volcano.jpg

```

Den er strukturert med tanke på MVC. Alle klasser som driver med «business logikk» ligger i pakken model.
Alle kontrollere ligger i pakken controller.
View mappen ligger ikke i Java mappen, men i Resources fordi view er fxml filene.

Alle units er i samme pakke, alle enums er i samme pakke og alle exceptions er i en pakke.
Dette er for å ha en ryddig pakkestruktur.

Det har blitt implementert 3 nye Units: AssassinUnit , PolarBearUniit og SpearmanUnit.
Assassinunit er en unit med mye damage og mindre armor. IceBearUnit er en kraftig unit.
Hvis den ikke har angrepet, er den «sulten» og for en stor angreps bonus.
Etter å ha angrepet en gang gjør den mindre skadebonus på påfølgende angrep fordi den er mett, men den får en forsvarsbonus.
Spearmanunit har bare ett spyd. Når den kaster det på noen, gjør den ekstremt mye skade.
Etter å ha kastet spydet har den ikke noe våpen og vil prøve å slå med knyttnevene.
Angreps metoden er refraktorert slik at hvis total angrepet ikke er større enn total forsvaret vil angriperen skade seg selv.
Dette er for å forhindre at programmet går i en uendelig loop.

Det ble implementert 3 nye terreng: Volcano, Dessert og Iceberg. Det ble implementert nye terrengbonuser til units.

I Battle er det lagt til en logg.
Denne i denne loggen ser man alle angrep og dødsfall.
Angrepsmeldinger i loggen baserer seg på hvilken type Unit som angriper og får et tilfeldig adverb.
Dødsmeldinger er tilfeldige.

Simuleringsmetoden bli splittet opp i to metoder.
Den ene kjører en runde av simuleringen mens den andre kjører en hel simulering.
Dette var for å få til saktere simulering i GUI.

For å få til sakte simulering har det blitt brukt timeline:

```
Timeline timeline = new Timeline();
timeline.setCycleCount(Timeline.INDEFINITE);
timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(speed.getValue().getRate()), actionEvent -> {
    if (!battle.isFinished()) {
        battle.singleSimulate();
        log.scrollTo(log.getItems().size());
        updateUnitInfo(0);
        updateUnitInfo(1);
        } else {
            timeline.stop();
            Winner.setText(battle.getWinner().getName() + " Wins!!!");
            Winner.setVisible(true);
            disableButtons(false);
       }
}));
timeline.playFromStart();
```

Timeline ses på av JavaFX som en animasjon som kan kjøres gjennom flere ganger.
Fordelen med å bruke dette i forhold til thread.sleep er at programmet ikke fryser imens simuleringen kjøres.

I Controller klassene har jeg brukt observer design mønstre for å oppdatere Bakgrunner og Listviews.
Slik har jeg implementert endring av bakgrunner.
```
terrain.getItems().setAll(Terrain.values());
terrain.getSelectionModel().selectedItemProperty().addListener((ov, t, t1) -> {
    switch (t1){
        case FOREST -> background.setId("forest");
        case HILL -> background.setId("hill");
        case PLAINS -> background.setId("plains");
        case DESERT -> background.setId("desert");
        case VOLCANO -> background.setId("volcano");
        case ICEBERG -> background.setId("iceberg");
    }
});
```
Når man endrer terreng, vil background som den bakerste borderpane bytte id i css filen til den som korresponderer med terrenget.
```
#forest {
    -fx-background-size: stretch, stretch;
    -fx-background-position: center;
    -fx-background-repeat: no-repeat;
    -fx-background-image: url(backgrounds/Forest.jpg);
}

#hill {
    -fx-background-size: stretch, stretch;
    -fx-background-position: center;
    -fx-background-repeat: no-repeat;
    -fx-background-image: url(backgrounds/Hill.jpg);
}

#plains {
    -fx-background-size: stretch, stretch;
    -fx-background-position: center;
    -fx-background-repeat: no-repeat;
    -fx-background-image: url(backgrounds/Plains.png);
}

#desert {
    -fx-background-size: stretch, stretch;
    -fx-background-position: center;
    -fx-background-repeat: no-repeat;
    -fx-background-image: url(backgrounds/Desert.jpg);
}

#volcano {
    -fx-background-size: stretch, stretch;
    -fx-background-position: center;
    -fx-background-repeat: no-repeat;
    -fx-background-image: url(backgrounds/Volcano.jpg);
}
#iceberg {
    -fx-background-size: stretch, stretch;
    -fx-background-position: center;
    -fx-background-repeat: no-repeat;
    -fx-background-image: url(backgrounds/Iceberg.jpg);
}
```
Fabrikk design mønster ble brukt i unitFactory. Denne klassen implanterer metoder for å enkelt lage Units.
Her ble det også brukt enums for å velge hva slags Units man vil ha.

Det er laget en egen klasse for å lese å skrive til fil.
Denne klassen heter filhandler. I denne klassen brukes FileWriters og BufferedReader.
Når man bruker dette, er det viktig å huske å lukke filen etter man er ferdig med den.
I koden er det brukt try with resource som lukker filen når den er ferdig.
Koden har også mange tester. Disse testene følger arrange act assert prinsippet.
Det er totalt 100 tester. Det er bare testet i Model fordi det det ikke er undervist i faget hvordan man tester GUI.

I koden har det også blitt brukt en del streams og lambda dette er en del av den funksjonelle programmeringen.

## Prosess
Det har blitt jobbet jevnt på programmet gjennom semesteret med unntak av når dette skjedde mye i andre fag.
Under utviklingen ble det laget enhets tester. Det har også blitt brukt versjon kontroll med Git.
Programmet er linket til et Repository på gitlab. Det har blitt satt opp en pipeline som builder og kjører testene hver gang man pusher endringer i programmet.
Under utvikling av ble det laget en wireframe. Programmet endte opp med å ligne på wireframen. Den ser slik ut:
![](ReportPictures/WireframeSample.png)


Programmet ble utviklet på en windows pc. grunnet dette kan det hende at andre operativsystem ikke har fonten som ble brukt.
Programmet er designet for å se slikt ut:
![](ReportPictures/Mainpage.png)
![](ReportPictures/Createpage.png)


Bildene i programmet er hentet fra https://pixabay.com. Disse bildene har en Pixabay License.
Det vil si at de er gratis for kommersiell bruk og det ikke er nødvendig å kreditere.

## Refleksjon
I programmet har jeg valgt å fokusere mer på simuleringen enn på GUI.
Jeg ønsket å lage et program som var enkel å bruke og derfor er GUI litt minimalistisk.
Ulempen med dette er at jeg kanskje ikke har fått vist all min kunnskap om GUI og JavaFX.
Jeg valgte å starte createArmy i ett nytt vindu istedenfor for å bytte scene.
Dette gjorde jeg fordi jeg ville gjøre det mulig å kunne lage armeer imens man kjørrer en lang simulering.

Controllerene mine kommuniserer ikke med hverandre.
Slike jeg designet det skal man lagre armer som CSV fil for deretter importere i på hovedsiden.
Jeg ville gjøre det slik fordi jeg ønsket å holde hovedsiden og createarmy siden uavhengig av hverandre.
Hvis jeg hadde lagt til funksjonalitet for å eksportere nye og redigerte armer til hovedsidene hadde jeg sendt dem ved å bruke singleton design mønster.

Siden jeg valgte å sette fokus på simuleringen har jeg fått utviklet den en del.
Jeg har lagt til flere Units og terreng som gjør at man kan ha mange flere unike slag.
Loggen til Battle er jeg veldig fornøyd med.
Den viser kanskje ikke mye programmeringsferdigheter, men jeg mener den forbedrer unnderholdningsverdien av programmet.

For å få kjøre simuleringen i reell tid hadde jeg 3 forskjellige ideer. Første ideen var å bruke thread.sleep.
Dette slet jeg med å få til å virke og det endte opp med at program frøs når den simulerte, noe som ikke var bra.
Den andre ideen min var å ha en klasse som logget absolutt alt som skjedde, hver eneste runde inkludert hvor mange units som var igjen.
Etter simuleringen spilte jeg av loggen i reprise ved hjelp av timeline. Denne løsningen fungerte
, men den var klønete og brukte mye minne og la til en del kobling i programmet.
Den tredje ideen var å bare bruke Timeline.
Jeg refaktorerte simulerings metoden inn i to metoder en for kun en runde og en for en hel simulering.
Jeg strukturerte deretter timeline slike at den i praksis virket som en while løkke.
Denne løsningen ble jeg veldig fornøyd med den fryser ikke hele programmet og bruker mye mindre minne. 

Når det kom til korrupte filer kunne jeg velge mellom å være streng og ikke laste inn en fil som har korrupt data eller å hoppe over de linjene som er korrupt og fortsette å legge til resten.
Jeg valgte å være streng, men jeg kunne også velge å hoppe over de korrupte delene og gi beskjed til brukeren om det.
Grunnen til jeg ikke valgte dette var fordi dette vil nok ha ført til at programmet fikk mer unødvendig kobling

I Army klassen gjorde jeg slik at det blir laget en observable list i konstruktøren.
Dette måtte jeg gjøre for å Listviews til å virke som det skulle.
Hvis jeg casted armylist til observable list uten denne endringen oppdaterte ikke listview seg uten brukeren samhandlet med dem.
Dette gjør at man må endre tilbake koden om man skulle brukte modellen til å lage et program som ikke bruker JavaFX.

Hadde jeg hatt mer tid til å utvikle programmet hadde jeg gjort en del ting.
Jeg hadde brukt SringProperty til å automatisk oppdatere hvor man ser antall av de forskjellige unitene.
Jeg hatt lagt til funksjonalitet til å lagre units i selve programmet med bruk av Serializable.

## Konklusjon

Jeg skulle lage et program for å simulere et slag mellom 2 armer og det skulle ha GUI med JavaFX. 
Jeg mener jeg har løst dette kravet. Jeg har lagt til mer funksjonalitet, som mer Units terreng og en mer avansert simuleringsmetode.
Jeg har fokusert mer på simuleringen en GUI og kanskje ikke vist all min kunnskap i JavaFX, men jeg har for vist en del.
Jeg har klart å bruke flere design mønstre, men jeg hadde brukt flere om jeg hadde hatt mer tid til programmet.
Jeg er svært fornøyd med hvordan slutt programmet ble seende ut og fungerer. Prosjektet har lært meg mye om programmering.
Det ligger ferdiglaget armer i mappen SampleArmy som kan brukes til å teste programmet.


