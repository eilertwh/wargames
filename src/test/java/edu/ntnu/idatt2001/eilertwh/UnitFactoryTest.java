package edu.ntnu.idatt2001.eilertwh;

import edu.ntnu.idatt2001.eilertwh.model.enums.UnitType;
import edu.ntnu.idatt2001.eilertwh.model.unitfactory.UnitFactory;
import edu.ntnu.idatt2001.eilertwh.model.units.Unit;
import edu.ntnu.idatt2001.eilertwh.model.units.specialized.*;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class UnitFactoryTest {
    @Nested
    public class Create_single_unit_Test{

        @Test
        public void create_infantry_unit_test(){
            Unit unit = UnitFactory.createUnit(UnitType.INFANTRY_UNIT,"Brage",10);
            assertTrue(unit instanceof InfantryUnit);
            assertEquals(10, unit.getHealth());
            assertEquals("Brage",unit.getName());
        }

        @Test
        public void create_ranged_unit_test(){
            Unit unit = UnitFactory.createUnit(UnitType.RANGED_UNIT,"Carl",9);
            assertTrue(unit instanceof RangedUnit);
            assertEquals(9, unit.getHealth());
            assertEquals("Carl",unit.getName());
        }

        @Test
        public void create_cavalry_unit_test(){
            Unit unit = UnitFactory.createUnit(UnitType.CAVALRY_UNIT,"Nicolai",8);
            assertTrue(unit instanceof CavalryUnit && ! (unit instanceof CommanderUnit));
            assertEquals(8, unit.getHealth());
            assertEquals("Nicolai",unit.getName());
        }

        @Test
        public void create_commander_unit_test(){
            Unit unit = UnitFactory.createUnit(UnitType.COMMANDER_UNIT,"Callum",7);
            assertTrue(unit instanceof CommanderUnit);
            assertEquals(7, unit.getHealth());
            assertEquals("Callum",unit.getName());
        }

        @Test
        public void create_Assassin_unit_test(){
            Unit unit = UnitFactory.createUnit(UnitType.ASSASSIN_UNIT,"Runar",6);
            assertTrue(unit instanceof AssassinUnit);
            assertEquals(6, unit.getHealth());
            assertEquals("Runar",unit.getName());
        }

        @Test
        public void create_polar_bear_unit_test(){
            Unit unit = UnitFactory.createUnit(UnitType.POLAR_BEAR_UNIT,"Trym",5);
            assertTrue(unit instanceof PolarBearUnit);
            assertEquals(5, unit.getHealth());
            assertEquals("Trym",unit.getName());
        }

        @Test
        public void create_spearman_unit_test(){
            Unit unit = UnitFactory.createUnit(UnitType.SPEARMAN_UNIT,"Tomas",4);
            assertTrue(unit instanceof SpearmanUnit);
            assertEquals(4, unit.getHealth());
            assertEquals("Tomas",unit.getName());
        }
    }

    @Nested
    public class Create_multiple_units_test{

        @Test
        public void create_infantry_unit_test(){
            List<Unit> units = UnitFactory.createUnits(25,UnitType.INFANTRY_UNIT,"Brage",10);
            assertEquals(25,units.size());
            assertFalse(units.stream().anyMatch(u -> !(u instanceof InfantryUnit)));
        }

        @Test
        public void create_ranged_unit_test(){
            List<Unit> units = UnitFactory.createUnits(24,UnitType.RANGED_UNIT,"Carl",9);
            assertEquals(24,units.size());
            assertFalse(units.stream().anyMatch(u -> !(u instanceof RangedUnit)));
        }

        @Test
        public void create_cavalry_unit_test(){
            List<Unit> units = UnitFactory.createUnits(23,UnitType.CAVALRY_UNIT,"Nicolai",8);
            assertEquals(23,units.size());
            assertFalse(units.stream().anyMatch(u -> !(u instanceof CavalryUnit) || u instanceof CommanderUnit));
        }

        @Test
        public void create_commander_unit_test(){
            List<Unit> units = UnitFactory.createUnits(22,UnitType.COMMANDER_UNIT,"Callum",7);
            assertEquals(22,units.size());
            assertFalse(units.stream().anyMatch(u -> !(u instanceof CommanderUnit)));
        }

        @Test
        public void create_Assassin_unit_test(){
            List<Unit> units = UnitFactory.createUnits(21,UnitType.ASSASSIN_UNIT,"Runar",6);
            assertEquals(21,units.size());
            assertFalse(units.stream().anyMatch(u -> !(u instanceof AssassinUnit)));
        }

        @Test
        public void create_polar_bear_unit_test(){
            List<Unit> units = UnitFactory.createUnits(20,UnitType.POLAR_BEAR_UNIT,"Trym",5);
            assertEquals(20,units.size());
            assertFalse(units.stream().anyMatch(u -> !(u instanceof PolarBearUnit)));
        }

        @Test
        public void create_spearman_unit_test(){
            List<Unit> units = UnitFactory.createUnits(19,UnitType.SPEARMAN_UNIT,"Tomas",4);
            assertEquals(19,units.size());
            assertFalse(units.stream().anyMatch(u -> !(u instanceof SpearmanUnit)));
        }
    }
}
